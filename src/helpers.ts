import { Just } from "purify-ts";

export function justDo<T>(fn: (value: T) => any) {
    return (value: T) => {
        fn(value);
        return Just(value)
    }
}