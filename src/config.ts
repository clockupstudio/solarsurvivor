import Phaser from "phaser";
import { palette } from "./colors";
import RexUIPlugin from "phaser3-rex-plugins/templates/ui/ui-plugin.js";

export default {
  type: Phaser.AUTO,
  parent: "game",
  backgroundColor: palette.darkest,
  zoom: 5,
  scale: {
    width: 160,
    height: 144,

    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  physics: {
    default: "arcade",
    arcade: {
      debug: false,
    },
  },
  render: {
    pixelArt: true,
  },
  plugins: {
    scene: [
      {
        key: "rexUI",
        plugin: RexUIPlugin,
        mapping: "rexUI",
      },
    ],
  },
};
