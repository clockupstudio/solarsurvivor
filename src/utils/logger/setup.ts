import debug from 'debug';

const gameLogger = debug('game:scene');

export function withLogging<T extends (...args: any[]) => any>(
    fn: T,
    name = fn.name
) {
    return (...args: Parameters<T>): ReturnType<T> => {
        console.log(`${name} called with:`, args);
        const result = fn(...args);
        console.log(`${name} returned:`, result);
        return result;
    };
} 