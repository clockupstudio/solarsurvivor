import config from './config';
import TitleScene from './scenes/title/title.scene';
import GameScene from './scenes/game/game.scene';
import PauseScene from './scenes/pause/pause.scene';
import Phaser from 'phaser';
import GameOverScene from './scenes/gameover/gameover.scene';


new Phaser.Game(
  Object.assign(config, {
    scene: [TitleScene, GameScene, PauseScene, GameOverScene]
  })
);
