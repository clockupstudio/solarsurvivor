import { Just } from "purify-ts";
import { justDo } from "./helpers";

describe('justDo', ()=>{
    test('execute the give function', ()=>{
        let fn = jest.fn();

        justDo(fn)({});

        expect(fn.mock.calls).toHaveLength(1);
    });

    test('should return Myabe of the given input', ()=>{
        let fn = (a: String) => {};

        let result = justDo(fn)('');

        expect(result).toEqual(Just(''));
    });
});
