import { PanelName } from "./panel/panel.contants";

export interface MenuItem {
    title: string;
    isSelected: boolean;
    id: string;
    subTitle: string;
    nextPanel: PanelName;
}

export const makeMenuItem = ({
    id = "",
    title = "",
    subTitle = "",
    isSelected = false,
    nextPanel = PanelName.NONE,
}: Partial<MenuItem> = {}) => {
    return {
        title: title,
        isSelected: isSelected,
        id: id,
        subTitle: subTitle,
        nextPanel: nextPanel,
    };
};
