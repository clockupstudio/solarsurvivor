export interface Cursor {
  x: number;
  y: number;
}

interface Countable {
  length: number;
}

export const convertToArrayIndex = (cursor: Cursor) => cursor.y * 2 + cursor.x;

export function isOverLeft(cursor: Cursor) {
  return cursor.x - 1 < 0;
}

export function isLastRow(point: Cursor, items: Countable) {
  return point.y === Math.round(items.length / 2) - 1;
}

export function isEven(items: Countable) {
  return items.length % 2 !== 0;
}

export function isOverRight(x: number) {
  return x + 1 > 1;
}

export function getLastRowIndex(items: Countable, currentColumn: number): number {
  if (currentColumn === 0) {
    return coutRow(items) - 1;
  }
  return isEven(items) ? coutRow(items) - 2 : coutRow(items) - 1;
}

export function coutRow(items: Countable) {
  return Math.round(items.length / 2);
}

export function isWillOverTop(y: number) {
  return y - 1 < 0;
}

export function isOnRightColumn(point: Cursor) {
  return point.x === 1;
}

export function isWillOverBottom(cursor: Cursor, items: Countable) {
  return cursor.y + 1 > getLastRowIndex(items, cursor.x)
}

export function indexToCursor(index: number, column: number): { x: number; y: number; } | undefined {
  return { x: index % column, y: Math.floor(index / column) };
}

