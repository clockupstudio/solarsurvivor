import { GameState, ICheckable, IExitable, ITriggerable } from '../game/interfaces';
import { Just, Maybe, Nothing } from 'purify-ts';
import { MenuControls } from './controls';
import {
  indexToCursor,
  convertToArrayIndex,
  isLastRow,
  isEven,
  isOverLeft,
  isOverRight,
  isWillOverTop,
  isOnRightColumn,
  getLastRowIndex,
  isWillOverBottom,
  coutRow,
} from './cursor';
import { PanelName, PanelType } from './panel/panel.contants';
import {
  hidePanel,
  initFirstWeaponPanelState,
  initSecondWeaponPanelState,
  initSlotPanelState,
  makePanelState,
  PanelState,
  showPanel,
} from './panel/panel.state';
import { makeMenuItem, MenuItem } from './menu.item';
import { selectItem } from './menu.state.select';
import {
  moveLeft as movePanelLeft,
  moveRight as movePanelRight,
  moveUp as movePanelUp,
  moveDown as movePanelDown,
} from './panel/panel.state.movement';

export enum MenuSound {
  SELECT,
  EXIT,
}

interface IMenuSoundPlayer {
  play(sound: MenuSound): void;
}
interface IMenuRenderer {
  render(): void;
}

interface IMenuControlsReader {
  readInput(): MenuControls[];
}

export interface MenuState {
  controls: MenuControls[];
  isCursorReady: boolean;
  scroll: number;
  life: number;
  coin: number;
  panelStates: PanelState[];
}

export const makeMenuState = ({
  controls = [],
  isCursorReady = true,
  scroll = 0,
  life = 0,
  coin = 0,
  panelStates = [],
}: Partial<MenuState> = {}) => {
  return {
    controls: controls,
    isCursorReady: isCursorReady,
    scroll: scroll,
    life: life,
    coin: coin,
    panelStates: panelStates,
  };
};

export interface IScrollable {
  scrollTo(percentage: number): void;
}

export interface IMenuScene
  extends ICheckable,
    ITriggerable,
    IExitable,
    IMenuControlsReader,
    IMenuRenderer,
    IMenuSoundPlayer,
    IScrollable {}

// InittialMenuState
// GameState-> MenuState
// { guns: [] } -> { menuItems: []}
// { gun: { title: 'x' }, guns: [ Gun{ title: 'x' } ] } -> { menuItems: [ { title: 'x', isSelected: true }]}
// { gun: { title: 'x' }, guns: [ Gun{ title: 'x' }, Gun{ title: 'y' } ] } -> { menuItems: [ { title: 'x', isSelected: true }, { title: 'y', isSelected: false }]}
// { gun: { title: 'y' }, guns: [ Gun{ title: 'x' }, Gun{ title: 'y' } ] } -> { menuItems: [ { title: 'x', isSelected: false }, { title: 'y', isSelected: true }]}
export const initMenuState = (gameState: GameState) => {
  return makeMenuState({
    life: gameState.player.life,
    coin: gameState.coins,
    panelStates: [
      initSlotPanelState(gameState),
      initFirstWeaponPanelState(gameState),
      initSecondWeaponPanelState(gameState),
    ],
  });
};

const applyAllMovements = (menuState: MenuState): Maybe<MenuState> => {
  return Just(menuState).chain(moveLeft).chain(moveRight).chain(moveUp).chain(moveDown);
};

export const updateMenuState = (scene: IMenuScene) => {
  return (state: MenuState) => {
    return Just(state)
      .chain(readDelayTimer(scene))
      .chain(skipOnDelayTimerProcessing)
      .chain(readControls(scene))
      .chain(applyAllMovements)
      .chain(selectItem)
      .chain(onPushExitButton(scene))
      .chain(renderMenu(scene))
      .chain(executeScroll(scene))
      .chain(playSound(scene))
      .chain(startDelayTimer(scene))
      .orDefault(state);
  };
};

// readControls
// MenuState, list of MenuControls -> MenuState
// makeMenuState(), [] -> makeMenuState()
// makeMenuState(menuItems:[makeMenuItem({id:})]), [ Select ] -> makeMenuState( selectedItem: makeGun())
export const readControls = (reader: IMenuControlsReader) => {
  return (menuState: MenuState) => Just({ ...menuState, controls: reader.readInput() });
};

const applyPanelMovement =
  (movement: (controls: MenuControls[]) => (panel: PanelState) => Maybe<PanelState>) =>
  (menuState: MenuState): Maybe<MenuState> => {
    return Just({
      ...menuState,
      panelStates: menuState.panelStates.map((panelState) =>
        panelState.isVisible
          ? movement(menuState.controls)(panelState).orDefault(panelState)
          : panelState
      ),
    });
  };

export const moveLeft = applyPanelMovement(movePanelLeft);
export const moveRight = applyPanelMovement(movePanelRight);
export const moveUp = applyPanelMovement(movePanelUp);
export const moveDown = applyPanelMovement(movePanelDown);

export const readDelayTimer = (timer: ICheckable) => {
  return (state: MenuState) => {
    return Just({ ...state, isCursorReady: timer.isReady });
  };
};

export const skipOnDelayTimerProcessing = (menuState: MenuState) => {
  return menuState.isCursorReady ? Just(menuState) : Nothing;
};

export const startDelayTimer = (timer: ITriggerable) => {
  return (state: MenuState) => {
    if (state.controls.length > 0) timer.start();
    return Just(state);
  };
};

export const renderMenu = (rednerer: IMenuRenderer) => {
  return (state: MenuState) => {
    rednerer.render();
    return Just(state);
  };
};

export const onPushExitButton = (scene: IExitable) => {
  return (state: MenuState) => {
    if (state.controls.includes(MenuControls.EXIT)) {
      console.log('onPushExitButton', state);
      scene.exit({
        weapon1Id: state.panelStates[1].selectedItem.id,
        weapon2Id: state.panelStates[2].selectedItem.id,
      });
    }
    return Just(state);
  };
};

const executeScroll = (scollable: IScrollable) => {
  return (menuState: MenuState) => {
    scollable.scrollTo(menuState.scroll);
    return Just(menuState);
  };
};

const playSound = (playable: IMenuSoundPlayer) => {
  return (menuState: MenuState) => {
    if (menuState.controls.includes(MenuControls.SELECT)) {
      playable.play(MenuSound.EXIT);
    } else if (menuState.controls.length > 0) {
      playable.play(MenuSound.SELECT);
    }

    return Just(menuState);
  };
};
