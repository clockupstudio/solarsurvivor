import { MenuControls } from './controls';
import { makeMenuItem } from './menu.item';
import { makeMenuState } from './menu.state';
import { selectItem } from './menu.state.select';
import { PanelType, PanelName } from './panel/panel.contants';
import { makePanelState } from './panel/panel.state';

describe('selectItem', () => {
  const BASE_SLOT_PANEL = makePanelState({
    panelType: PanelType.SLOT_PANEL,
    uiName: 'slot-panel',
    isVisible: false,
    items: [
      makeMenuItem({
        id: 'slot-1',
        title: 'Weapon 1',
        subTitle: '',
        nextPanel: PanelName.WEAPON_1_PANEL,
      }),
      makeMenuItem({
        id: 'slot-2',
        title: 'Weapon 2',
        subTitle: '',
        nextPanel: PanelName.WEAPON_2_PANEL,
      }),
    ],
  });

  const BASE_WEAPON_1_PANEL = makePanelState({
    panelType: PanelType.WEAPON_PANEL,
    uiName: PanelName.WEAPON_1_PANEL,
    isVisible: false,
    items: [
      makeMenuItem({ id: 'gun-1', title: 'Gun 1', isSelected: false }),
      makeMenuItem({ id: 'gun-2', title: 'Gun 2', isSelected: false }),
    ],
  });

  const BASE_WEAPON_2_PANEL = makePanelState({
    panelType: PanelType.WEAPON_PANEL,
    uiName: PanelName.WEAPON_2_PANEL,
    isVisible: false,
    items: [
      makeMenuItem({ id: 'gun-1', title: 'Gun 1', isSelected: false }),
      makeMenuItem({ id: 'gun-2', title: 'Gun 2', isSelected: false }),
    ],
  });

  const BASE_MENU_STATE = makeMenuState({
    panelStates: [BASE_SLOT_PANEL, BASE_WEAPON_1_PANEL, BASE_WEAPON_2_PANEL],
    controls: [MenuControls.SELECT],
  });

  describe('basic controls', () => {
    it('should return same state when no controls present', () => {
      expect(selectItem(makeMenuState()).extract()).toEqual(makeMenuState());
    });

    it('should return same state when SELECT not present', () => {
      const state = makeMenuState({ controls: [MenuControls.DOWN] });
      expect(selectItem(state).extract()).toEqual(state);
    });
  });

  describe('slot panel workflow', () => {
    const slotPanelState = makeMenuState({
      controls: [MenuControls.SELECT],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: PanelName.SLOT_PANEL,
          isVisible: true,
          items: [
            makeMenuItem({
              id: 'slot-1',
              title: 'Weapon 1',
              isSelected: true,
              nextPanel: PanelName.WEAPON_1_PANEL,
            }),
          ],
        }),
        makePanelState({
          panelType: PanelType.WEAPON_PANEL,
          uiName: PanelName.WEAPON_1_PANEL,
          isVisible: false,
          items: [makeMenuItem({ id: 'gun-1', title: 'Gun 1' })],
        }),
      ],
    });

    it('should transition from slot to weapon panel', () => {
      const result = selectItem(slotPanelState).extract();

      expect(result?.panelStates[0].isVisible).toBeFalsy();
      expect(result?.panelStates[1].isVisible).toBeTruthy();
    });

    it('should select first item in weapon panel when transitioning', () => {
      const result = selectItem(slotPanelState).extract();
      expect(result?.panelStates[1].items[0].isSelected).toBeTruthy();
    });

    it('should handle multiple weapon slots', () => {
      const stateWithMultipleSlots = makeMenuState({
        controls: [MenuControls.SELECT],
        panelStates: [
          makePanelState({
            panelType: PanelType.SLOT_PANEL,
            uiName: PanelName.SLOT_PANEL,
            isVisible: true,
            items: [
              makeMenuItem({ id: 'slot-1', nextPanel: PanelName.WEAPON_1_PANEL }),
              makeMenuItem({ id: 'slot-2', isSelected: true, nextPanel: PanelName.WEAPON_2_PANEL }),
            ],
          }),
          makePanelState({ uiName: PanelName.WEAPON_1_PANEL, isVisible: false }),
          makePanelState({ uiName: PanelName.WEAPON_2_PANEL, isVisible: false }),
        ],
      });

      const result = selectItem(stateWithMultipleSlots).extract();
      expect(result?.panelStates[2].isVisible).toBeTruthy();
    });
  });

  describe('weapon panel workflow', () => {
    const weaponPanelState = makeMenuState({
      controls: [MenuControls.SELECT],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: PanelName.SLOT_PANEL,
          isVisible: false,
          items: [
            makeMenuItem({ 
              id: 'slot-1', 
              nextPanel: PanelName.WEAPON_1_PANEL,
              subTitle: 'Old Gun' 
            }),
            makeMenuItem({ 
              id: 'slot-2', 
              nextPanel: PanelName.WEAPON_2_PANEL,
              subTitle: 'Different Gun' 
            }),
          ],
        }),
        makePanelState({
          panelType: PanelType.WEAPON_PANEL,
          uiName: PanelName.WEAPON_1_PANEL,
          isVisible: true,
          selectedItem: makeMenuItem({ id: 'gun-1', title: 'Old Gun' }),
          items: [
            makeMenuItem({ id: 'gun-1', title: 'Old Gun' }),
            makeMenuItem({ id: 'gun-2', title: 'New Gun', isSelected: true }),
          ],
        }),
      ],
    });

    it('should update weapon panel selected item', () => {
      const result = selectItem(weaponPanelState).extract();
      expect(result?.panelStates[1].selectedItem.title).toBe('New Gun');
    });

    it('should update only matching slot subtitle', () => {
      const result = selectItem(weaponPanelState).extract();
      expect(result?.panelStates[0].items[0].subTitle).toBe('New Gun');
      expect(result?.panelStates[0].items[1].subTitle).toBe('Different Gun');
    });
  });
});
