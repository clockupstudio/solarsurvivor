import { Just, Maybe } from "purify-ts";
import { MenuControls } from "../controls";
import { PanelState } from "./panel.state";
import { isLastRow, isEven, isOverLeft, isOverRight, isWillOverTop, isOnRightColumn, getLastRowIndex, isWillOverBottom } from "../cursor";
import { updatePanelCursor } from "./panel.state";

// Types and interfaces
interface GridConstants {
  LEFT_EDGE: number;
  RIGHT_EDGE: number;
  TOP_EDGE: number;
}

interface MovementConditions {
  shouldStayAtLeftEdge(panel: PanelState): boolean;
  shouldWrapToRightEdge(x: number): boolean;
  shouldWrapToLeftEdge(x: number): boolean;
  shouldWrapToBottom(panel: PanelState): boolean;
}

// Implementation details (need to be first)
const GRID: GridConstants = {
  LEFT_EDGE: 0,
  RIGHT_EDGE: 1,
  TOP_EDGE: 0
};

const movementConditions: MovementConditions = {
  shouldStayAtLeftEdge: (panel) => 
    isLastRow(panel.cursor, panel.items) && 
    isEven(panel.items) && 
    isOverLeft(panel.cursor),

  shouldWrapToRightEdge: (x) => 
    x - 1 < GRID.LEFT_EDGE,

  shouldWrapToLeftEdge: (x) => 
    isOverRight(x),

  shouldWrapToBottom: (panel) =>
    isWillOverTop(panel.cursor.y) ||
    (isWillOverTop(panel.cursor.y) && isOnRightColumn(panel.cursor))
};

// Types
type PositionCalculator = (panel: PanelState) => { x?: number; y?: number };
type MovementFn = (panel: PanelState) => Maybe<PanelState>;

// Pure functions for movement steps
const hasControl = (control: MenuControls, controls: MenuControls[]): boolean => 
  controls.includes(control);

const updateCursorPosition = (
  panel: PanelState,
  newPosition: { x?: number; y?: number }
): PanelState => ({
  ...panel,
  cursor: { 
    ...panel.cursor,
    ...newPosition
  }
});

// Compose movement function
const applyMovement = (
  control: MenuControls,
  calculatePosition: PositionCalculator
) => (controls: MenuControls[]): MovementFn => 
  (panel) => !hasControl(control, controls)
    ? Just(panel)
    : Just(panel)
        .map(p => updateCursorPosition(p, calculatePosition(p)))
        .map(updatePanelCursor);

// Public movement API
export const moveLeft = applyMovement(MenuControls.LEFT, (panel) => ({
  x: movementConditions.shouldStayAtLeftEdge(panel)
    ? GRID.LEFT_EDGE
    : movementConditions.shouldWrapToRightEdge(panel.cursor.x) 
      ? GRID.RIGHT_EDGE 
      : GRID.LEFT_EDGE
}));

export const moveRight = applyMovement(MenuControls.RIGHT, (panel) => ({
  x: isLastRow(panel.cursor, panel.items) && isEven(panel.items)
    ? GRID.LEFT_EDGE
    : movementConditions.shouldWrapToLeftEdge(panel.cursor.x) 
      ? GRID.LEFT_EDGE 
      : panel.cursor.x + 1
}));

export const moveUp = applyMovement(MenuControls.UP, (panel) => ({
  y: movementConditions.shouldWrapToBottom(panel)
    ? getLastRowIndex(panel.items, panel.cursor.x)
    : panel.cursor.y - 1
}));

export const moveDown = applyMovement(MenuControls.DOWN, (panel) => ({
  y: isWillOverBottom(panel.cursor, panel.items)
    ? GRID.TOP_EDGE
    : panel.cursor.y + 1
}));