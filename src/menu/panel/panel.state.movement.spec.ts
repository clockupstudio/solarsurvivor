import { MenuControls } from "../controls";
import { makeMenuItem } from "../menu.item";
import { makePanelState } from "./panel.state";
import { moveLeft, moveRight, moveUp, moveDown } from "./panel.state.movement";
import { PanelType } from "./panel.contants";

describe("moveLeft", () => {
  it("should not move when no LEFT control", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false })
      ]
    });

    expect(moveLeft([MenuControls.RIGHT])(panel).extract()).toEqual(panel);
  });

  it("should move cursor left and update selected item", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 1, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: true })
      ]
    });

    expect(moveLeft([MenuControls.LEFT])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false })
      ]
    });
  });

  it("should wrap to right and update selected item", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false })
      ]
    });

    expect(moveLeft([MenuControls.LEFT])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 1, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: true })
      ]
    });
  });

  it("should not wrap on last row with odd number of items and keep selection", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });

    expect(moveLeft([MenuControls.LEFT])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });
  });
});

describe("moveRight", () => {
  it("should not move when no RIGHT control", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false })
      ]
    });

    expect(moveRight([MenuControls.LEFT])(panel).extract()).toEqual(panel);
  });

  it("should move cursor right and update selected item", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false })
      ]
    });

    expect(moveRight([MenuControls.RIGHT])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 1, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: true })
      ]
    });
  });

  it("should wrap to left when at rightmost position", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 1, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: true })
      ]
    });

    expect(moveRight([MenuControls.RIGHT])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false })
      ]
    });
  });

  it("should not move on last row with odd number of items", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });

    expect(moveRight([MenuControls.RIGHT])(panel).extract()).toEqual(panel);
  });
});

describe("moveUp", () => {
  it("should not move when no UP control", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });

    expect(moveUp([MenuControls.DOWN])(panel).extract()).toEqual(panel);
  });

  it("should move cursor up and update selected item", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });

    expect(moveUp([MenuControls.UP])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: false })
      ]
    });
  });

  it("should wrap to bottom when at top", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: false })
      ]
    });

    expect(moveUp([MenuControls.UP])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });
  });
});

describe("moveDown", () => {
  it("should not move when no DOWN control", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: false })
      ]
    });

    expect(moveDown([MenuControls.UP])(panel).extract()).toEqual(panel);
  });

  it("should move cursor down and update selected item", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: false })
      ]
    });

    expect(moveDown([MenuControls.DOWN])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });
  });

  it("should wrap to top when at bottom", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });

    expect(moveDown([MenuControls.DOWN])(panel).extract()).toEqual({
      ...panel,
      cursor: { x: 0, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: true }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: false })
      ]
    });
  });
}); 