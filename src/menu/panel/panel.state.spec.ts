import { makeGun } from "../../game/character/bullet";
import { GameState } from "../../game/interfaces";
import { makeGameState } from "../../game/states/gamestate";
import { makeMenuItem } from "../menu.item";
import { PanelType, PanelName } from "./panel.contants";
import { hidePanel, initFirstWeaponPanelState, initSecondWeaponPanelState, initSlotPanelState, makePanelState, PanelState, showPanel, updatePanelCursor } from "./panel.state";

describe("initSlotPanelState", () => {
    it("should return a panel state with the correct properties and default select first item", () => {
        const panelState: PanelState = initSlotPanelState(makeGameState());
        expect(panelState.panelType).toBe(PanelType.SLOT_PANEL);
        expect(panelState.uiName).toBe(PanelName.SLOT_PANEL);
        expect(panelState.isVisible).toBe(true);
        expect(panelState.items[0].isSelected).toBe(true);
        expect(panelState.items[1].isSelected).toBe(false);
    });
});

describe("initFirstWeaponPanelState", () => {
    it("should return a weapon panel state with items and selected first item from current game state", () => {
        const gameState: GameState = makeGameState({
            weaponSlots: {
                primary: makeGun({ id: "gun-1", name: "Gun 1" }),
                secondary: makeGun({ id: "gun-2", name: "Gun 2" }),
            },
            guns: [
                makeGun({ id: "gun-1", name: "Gun 1" }),
                makeGun({ id: "gun-2", name: "Gun 2" }),
            ]
        });
        const panelState: PanelState = initFirstWeaponPanelState(gameState);
        expect(panelState.panelType).toBe(PanelType.WEAPON_PANEL);
        expect(panelState.uiName).toBe(PanelName.WEAPON_1_PANEL);
        expect(panelState.isVisible).toBe(false);
        expect(panelState.items).toEqual([
            makeMenuItem({ id: "gun-1", title: "Gun 1", subTitle: "", isSelected: false, nextPanel: PanelName.SLOT_PANEL }),
            makeMenuItem({ id: "gun-2", title: "Gun 2", subTitle: "", isSelected: false, nextPanel: PanelName.SLOT_PANEL }),
        ]);
        expect(panelState.selectedItem).toEqual(panelState.items[0]);
    });

    it("should return a weapon panel state with items and selected second item from current game state", () => {
      const gameState: GameState = makeGameState({
          weaponSlots: {
              primary: makeGun({ id: "gun-2", name: "Gun 2" }),
              secondary: makeGun({ id: "gun-1", name: "Gun 1" }),
          },
          guns: [
              makeGun({ id: "gun-1", name: "Gun 1" }),
              makeGun({ id: "gun-2", name: "Gun 2" }),
          ]
      });
      const panelState: PanelState = initFirstWeaponPanelState(gameState);
      expect(panelState.panelType).toBe(PanelType.WEAPON_PANEL);
      expect(panelState.uiName).toBe(PanelName.WEAPON_1_PANEL);
      expect(panelState.isVisible).toBe(false);
      expect(panelState.items).toEqual([
          makeMenuItem({ id: "gun-1", title: "Gun 1", subTitle: "", isSelected: false, nextPanel: PanelName.SLOT_PANEL }),
          makeMenuItem({ id: "gun-2", title: "Gun 2", subTitle: "", isSelected: false, nextPanel: PanelName.SLOT_PANEL }),
      ]);
      expect(panelState.selectedItem).toEqual(panelState.items[1]);
  });
});

describe("initSecondWeaponPanelState", () => {
    it("should return a weapon panel state with items and selected from current game state", () => {
        const gameState: GameState = makeGameState({
            weaponSlots: {
                primary: makeGun({ id: "gun-1", name: "Gun 1" }),
                secondary: makeGun({ id: "gun-2", name: "Gun 2" }),
            },
            guns: [
                makeGun({ id: "gun-1", name: "Gun 1" }),
                makeGun({ id: "gun-2", name: "Gun 2" }),
            ]
        });
        const panelState: PanelState = initSecondWeaponPanelState(gameState);
        expect(panelState.panelType).toBe(PanelType.WEAPON_PANEL);
        expect(panelState.uiName).toBe(PanelName.WEAPON_2_PANEL);
        expect(panelState.isVisible).toBe(false);
        expect(panelState.items).toEqual([
            makeMenuItem({ id: "gun-1", title: "Gun 1", subTitle: "", isSelected: false, nextPanel: PanelName.SLOT_PANEL }),
            makeMenuItem({ id: "gun-2", title: "Gun 2", subTitle: "", isSelected: false, nextPanel: PanelName.SLOT_PANEL }),
        ]);
    });
});

describe("hidePanel", () => {
    it("should hide the panel", () => {
        const panelState: PanelState = hidePanel(makePanelState({
            items: [
                makeMenuItem({ id: "slot-1", title: "Weapon 1", subTitle: "G2", isSelected: true }),
                makeMenuItem({ id: "slot-2", title: "Weapon 2", subTitle: "G2", isSelected: false }),
            ]
        }));
        expect(panelState.isVisible).toBe(false);
        expect(panelState.items[0].isSelected).toBe(false);
        expect(panelState.items[1].isSelected).toBe(false);
    });
});

describe("showPanel", () => {
    it("should show the panel", () => {
        const panelState: PanelState = showPanel(makePanelState({
            items: [
                makeMenuItem({ id: "slot-1", title: "Weapon 1", subTitle: "G2", isSelected: false }),
                makeMenuItem({ id: "slot-2", title: "Weapon 2", subTitle: "G2", isSelected: false }),
            ]
        }));
        expect(panelState.isVisible).toBe(true);
        expect(panelState.items[0].isSelected).toBe(true);
        expect(panelState.items[1].isSelected).toBe(false);
    });
});

describe("updatePanelCursor", () => {
  it("should update selected item based on cursor position", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 1, y: 0 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false })
      ]
    });

    expect(updatePanelCursor(panel)).toEqual({
      ...panel,
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: true })
      ]
    });
  });

  it("should update selected item on second row", () => {
    const panel = makePanelState({
      panelType: PanelType.SLOT_PANEL,
      uiName: "test",
      cursor: { x: 0, y: 1 },
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: false })
      ]
    });

    expect(updatePanelCursor(panel)).toEqual({
      ...panel,
      items: [
        makeMenuItem({ id: "1", isSelected: false }),
        makeMenuItem({ id: "2", isSelected: false }),
        makeMenuItem({ id: "3", isSelected: true })
      ]
    });
  });
});

