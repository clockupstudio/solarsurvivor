
export enum PanelType {
    SLOT_PANEL,
    WEAPON_PANEL,
}

export enum PanelName {
    SLOT_PANEL = "slot-panel",
    WEAPON_1_PANEL = "weapon-1-panel",
    WEAPON_2_PANEL = "weapon-2-panel",
    NONE = "NONE",
}
