import { Maybe } from 'purify-ts';
import { GameState } from '../../game/interfaces';
import { convertToArrayIndex } from '../cursor';
import { makeMenuItem, MenuItem } from '../menu.item';
import { PanelName, PanelType } from './panel.contants';

export interface PanelState {
  cursor: { x: number; y: number };
  panelType: PanelType;
  uiName: string;
  isVisible: boolean;
  items: MenuItem[];
  selectedItem: MenuItem;
}

export const makePanelState = ({
  panelType = PanelType.SLOT_PANEL,
  uiName = '',
  isVisible = true,
  items = [],
  cursor = { x: 0, y: 0 },
  selectedItem = makeMenuItem({ id: '', title: '', subTitle: '', isSelected: false }),
}: Partial<PanelState> = {}) => {
  return { panelType, uiName, isVisible, items, cursor, selectedItem };
};

export const initSlotPanelState = (gameState: GameState): PanelState => {
  const menuItems = [
    makeMenuItem({
      id: 'slot-1',
      title: 'Weapon 1',
      subTitle: gameState.weaponSlots.primary.name,
      isSelected: true,
      nextPanel: PanelName.WEAPON_1_PANEL,
    }),
    makeMenuItem({
      id: 'slot-2',
      title: 'Weapon 2',
      subTitle: gameState.weaponSlots.secondary.name,
      isSelected: false,
      nextPanel: PanelName.WEAPON_2_PANEL,
    }),
  ];
  return makePanelState({
    panelType: PanelType.SLOT_PANEL,
    uiName: PanelName.SLOT_PANEL,
    isVisible: true,
    items: menuItems,
    selectedItem: menuItems[0],
  });
};

export const initFirstWeaponPanelState = (gameState: GameState): PanelState => {
  const items = gameState.guns.map((gun) =>
    makeMenuItem({
      id: gun.id,
      title: gun.name,
      subTitle: '',
      isSelected: false,
      nextPanel: PanelName.SLOT_PANEL,
    })
  );
  return makePanelState({
    panelType: PanelType.WEAPON_PANEL,
    uiName: PanelName.WEAPON_1_PANEL,
    isVisible: false,
    items: items,
    selectedItem: items.find((item) => item.id === gameState.weaponSlots.primary.id) ?? items[0],
  });
};

export const initSecondWeaponPanelState = (gameState: GameState): PanelState => {
  return makePanelState({
    panelType: PanelType.WEAPON_PANEL,
    uiName: PanelName.WEAPON_2_PANEL,
    isVisible: false,
    items: gameState.guns.map((gun) =>
      makeMenuItem({
        id: gun.id,
        title: gun.name,
        subTitle: '',
        isSelected: false,
        nextPanel: PanelName.SLOT_PANEL,
      })
    ),
  });
};

export const hidePanel = (panelState: PanelState) => {
  return {
    ...panelState,
    isVisible: false,
    items: panelState.items.map((item) => ({ ...item, isSelected: false })),
  };
};

export const showPanel = (panelState: PanelState) => {
  return {
    ...panelState,
    isVisible: true,
    items: panelState.items.map((item, index) => ({
      ...item,
      isSelected: index === convertToArrayIndex(panelState.cursor),
    })),
  };
};

export const updatePanelCursor = (panelState: PanelState): PanelState => {
  return {
    ...panelState,
    items: panelState.items.map((item, index) => ({
      ...item,
      isSelected: index === convertToArrayIndex(panelState.cursor),
    })),
  };
};

export const findPanelByName =
  (panels: PanelState[]) =>
  (name: PanelName): Maybe<PanelState> =>
    Maybe.fromNullable(panels.find((p) => p.uiName === name));
