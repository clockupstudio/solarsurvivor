import { indexToCursor } from "./cursor"

describe('indexToCursor', () => {
    it('given 0 in 2 columns return first co-ordinate', () => {
        expect(indexToCursor(0, 2)).toEqual({ x: 0, y: 0 });
    });

    it('given 1 in 2 columns return second co-ordinate', () => {
        expect(indexToCursor(1, 2)).toEqual({ x: 1, y: 0 });
    });

    it('given 2 in 2 columns return third co-ordinate', () => {
        expect(indexToCursor(2, 2)).toEqual({ x: 0, y: 1 });
    });
});