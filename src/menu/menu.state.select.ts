import { Maybe, Just, Nothing } from 'purify-ts';
import { MenuControls } from './controls';
import { MenuState } from './menu.state';
import { PanelState, showPanel, hidePanel } from './panel/panel.state';
import { PanelType, PanelName } from './panel/panel.contants';

export const selectItem = (state: MenuState): Maybe<MenuState> =>
  hasSelectControl(state)
    .chain(findVisiblePanel)
    .chain(handlePanelSelection)
    .chain(updatePanelVisibility)
    .alt(Just(state));

const hasSelectControl = (state: MenuState): Maybe<MenuState> =>
  Maybe.fromPredicate(() => state.controls.includes(MenuControls.SELECT), state);

const findVisiblePanel = (state: MenuState) =>
  Maybe.fromNullable(state.panelStates.find((s: PanelState) => s.isVisible)).map((panel) => ({
    state,
    panel,
  }));

const handlePanelSelection = ({ state, panel }: { state: MenuState; panel: PanelState }) =>
  panel.panelType === PanelType.WEAPON_PANEL
    ? handleWeaponPanelSelect(panel, state)
    : handleSlotPanelSelect(panel, state);

const handleSlotPanelSelect = (panel: PanelState, state: MenuState) =>
  getNextPanelFromSelectedItem(panel).map((nextPanelName) => ({ state, nextPanelName }));

const getNextPanelFromSelectedItem = (panel: PanelState): Maybe<PanelName> =>
  Maybe.fromNullable(panel.items.find((item) => item.isSelected)?.nextPanel);

const updatePanelInState = (panel: PanelState) => (state: MenuState): MenuState => ({
  ...state,
  panelStates: state.panelStates.map(p => 
    p.uiName === panel.uiName ? panel : p
  )
});

const getWeaponTitle = (panel: PanelState) => 
  panel.selectedItem.title;

const handleWeaponPanelSelect = (panel: PanelState, state: MenuState) => 
  Just(panel)
    .map(updateWeaponPanelSelectedItem)
    .map(updatedPanel => ({
      panel: updatedPanel,
      state: updatePanelInState(updatedPanel)(state)
    }))
    .map(({panel, state}) => ({
      state: updateSlotSubtitleFromWeapon(
        getWeaponTitle(panel),
        panel.uiName as PanelName
      )(state),
      nextPanelName: PanelName.SLOT_PANEL
    }));

const updateWeaponPanelSelectedItem = (panel: PanelState): PanelState => {
  const selectedItem = panel.items.find((item) => item.isSelected);
  return {
    ...panel,
    selectedItem: selectedItem ?? panel.selectedItem,
  };
};

const updateSlotSubtitleFromWeapon =
  (weaponTitle: string, panelName: PanelName) =>
  (state: MenuState): MenuState => ({
    ...state,
    panelStates: state.panelStates.map((p) =>
      p.uiName === PanelName.SLOT_PANEL
        ? {
            ...p,
            items: p.items.map((item) => ({
              ...item,
              subTitle: item.nextPanel === panelName ? weaponTitle : item.subTitle,
            })),
          }
        : p
    ),
  });

const panelStateMap = {
  isMatch: (panelName: string) => (s: PanelState) => s.uiName === panelName,

  match: (panelName: string) => (s: PanelState) =>
    panelStateMap.isMatch(panelName)(s) ? showPanel(s) : hidePanel(s),
};

const updatePanelVisibility = ({
  state,
  nextPanelName,
}: {
  state: MenuState;
  nextPanelName: string;
}) =>
  Maybe.fromNullable(state.panelStates.find(panelStateMap.isMatch(nextPanelName))).map(() => ({
    ...state,
    panelStates: state.panelStates.map(panelStateMap.match(nextPanelName)),
  }));
