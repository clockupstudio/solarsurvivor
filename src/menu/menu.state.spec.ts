import { makeGameState } from '../game/states/gamestate';
import { MenuControls } from './controls';
import { makeMenuItem } from './menu.item';
import {
  makeMenuState,
  moveDown,
  moveLeft,
  moveRight,
  moveUp,
  readControls,
} from './menu.state';
import { PanelName } from './panel/panel.contants';
import { makePanelState } from './panel/panel.state';
import { PanelType } from './panel/panel.contants';

describe('InitMenuState', () => {

});

describe('readControls', () => {
  it('empty controls should return same sate', () => {
    const reader = { readInput: () => [] };
    expect(readControls(reader)(makeMenuState()).extract()).toEqual(makeMenuState());
  });

  it('should put list controls into state', () => {
    const reader = { readInput: () => [MenuControls.DOWN] };
    expect(readControls(reader)(makeMenuState()).extract()).toEqual(
      makeMenuState({ controls: [MenuControls.DOWN] })
    );
  });
});

describe('moveLeft', () => {
  it('empty controls returns same state', () => {
    expect(moveLeft(makeMenuState()).extract()).toEqual(makeMenuState());
  });

  it('should not move when no visible panels', () => {
    const state = makeMenuState({
      controls: [MenuControls.LEFT],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: false,
          items: [makeMenuItem(), makeMenuItem()],
        }),
      ],
    });
    expect(moveLeft(state).extract()).toEqual(state);
  });

  it('should move cursor left in visible panel', () => {
    const state = makeMenuState({
      controls: [MenuControls.LEFT],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 1, y: 0 },
          items: [
            makeMenuItem({ id: '1', isSelected: false }),
            makeMenuItem({ id: '2', isSelected: true }),
          ],
        }),
      ],
    });

    expect(moveLeft(state).extract()).toEqual({
      ...state,
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 0, y: 0 },
          items: [
            makeMenuItem({ id: '1', isSelected: true }),
            makeMenuItem({ id: '2', isSelected: false }),
          ],
        }),
      ],
    });
  });
});

describe('moveRight', () => {
  it('empty controls returns same state', () => {
    expect(moveRight(makeMenuState()).extract()).toEqual(makeMenuState());
  });

  it('should not move when no visible panels', () => {
    const state = makeMenuState({
      controls: [MenuControls.RIGHT],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: false,
          items: [makeMenuItem(), makeMenuItem()],
        }),
      ],
    });
    expect(moveRight(state).extract()).toEqual(state);
  });

  it('should move cursor right in visible panel', () => {
    const state = makeMenuState({
      controls: [MenuControls.RIGHT],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 0, y: 0 },
          items: [
            makeMenuItem({ id: '1', isSelected: true }),
            makeMenuItem({ id: '2', isSelected: false }),
          ],
        }),
      ],
    });

    expect(moveRight(state).extract()).toEqual({
      ...state,
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 1, y: 0 },
          items: [
            makeMenuItem({ id: '1', isSelected: false }),
            makeMenuItem({ id: '2', isSelected: true }),
          ],
        }),
      ],
    });
  });
});

describe('moveUp', () => {
  it('empty controls returns same state', () => {
    expect(moveUp(makeMenuState()).extract()).toEqual(makeMenuState());
  });

  it('should not move when no visible panels', () => {
    const state = makeMenuState({
      controls: [MenuControls.UP],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: false,
          items: [makeMenuItem(), makeMenuItem(), makeMenuItem()],
        }),
      ],
    });
    expect(moveUp(state).extract()).toEqual(state);
  });

  it('should move cursor up in visible panel', () => {
    const state = makeMenuState({
      controls: [MenuControls.UP],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 0, y: 1 },
          items: [
            makeMenuItem({ id: '1', isSelected: false }),
            makeMenuItem({ id: '2', isSelected: false }),
            makeMenuItem({ id: '3', isSelected: true }),
          ],
        }),
      ],
    });

    expect(moveUp(state).extract()).toEqual({
      ...state,
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 0, y: 0 },
          items: [
            makeMenuItem({ id: '1', isSelected: true }),
            makeMenuItem({ id: '2', isSelected: false }),
            makeMenuItem({ id: '3', isSelected: false }),
          ],
        }),
      ],
    });
  });
});

describe('moveDown', () => {
  it('empty controls returns same state', () => {
    expect(moveDown(makeMenuState()).extract()).toEqual(makeMenuState());
  });

  it('should not move when no visible panels', () => {
    const state = makeMenuState({
      controls: [MenuControls.DOWN],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: false,
          items: [makeMenuItem(), makeMenuItem(), makeMenuItem()],
        }),
      ],
    });
    expect(moveDown(state).extract()).toEqual(state);
  });

  it('should move cursor down in visible panel', () => {
    const state = makeMenuState({
      controls: [MenuControls.DOWN],
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 0, y: 0 },
          items: [
            makeMenuItem({ id: '1', isSelected: true }),
            makeMenuItem({ id: '2', isSelected: false }),
            makeMenuItem({ id: '3', isSelected: false }),
          ],
        }),
      ],
    });

    expect(moveDown(state).extract()).toEqual({
      ...state,
      panelStates: [
        makePanelState({
          panelType: PanelType.SLOT_PANEL,
          uiName: 'test',
          isVisible: true,
          cursor: { x: 0, y: 1 },
          items: [
            makeMenuItem({ id: '1', isSelected: false }),
            makeMenuItem({ id: '2', isSelected: false }),
            makeMenuItem({ id: '3', isSelected: true }),
          ],
        }),
      ],
    });
  });
});
