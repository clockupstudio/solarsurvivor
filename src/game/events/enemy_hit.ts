import {Character} from "../character/character";
import {findEventsByName, GameEvent, GameEventNames, getEventData, makeGameEvent} from "./event";

export interface EnemyHitEventData {
    id: String;
    damage: number;
}

export function makeEnemyHitEvent(data: EnemyHitEventData) {
    return makeGameEvent(GameEventNames.ENEMY_HIT, data);
}

export function getListOfEnemyHitData(events: GameEvent[]) {
    return findEventsByName(events, GameEventNames.ENEMY_HIT)
        .map((enemyHit)=> getEventData<EnemyHitEventData>(enemyHit));
}

export function makeEnemyRecoveredEvent(id: string) {
    return makeGameEvent(GameEventNames.ENEMY_RECOVERED, {id: id});
}
