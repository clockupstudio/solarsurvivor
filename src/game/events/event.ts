import { Just, Maybe, Nothing } from "purify-ts";
import { Character } from "../character/character";

export enum GameEventNames {
  SELECTED_GUN = "SELECTED_GUN",
  ENEMY_HIT = "ENEMY_HIT",
  PLAYER_HIT = "PLAYER_HIT",
  PLAYER_RECOVERED = "PLAYER_RECOVERED",
  PLAYER_DEAD_ANIMATION_END = "PLAYER_DEAD_ANIMATION_END",
  ENEMY_SPAWN = "ENEMY_SPAWN",
  ENEMY_RECOVERED = "ENEMY_RECOVERED",
  ITEM_HIT = "ITEM_HIT",
  ENEMY_DEAD = "ENEMY_DEAD",
  WEAPON_READY = "WEAPON_READY",
}

export interface GameEvent {
  name: GameEventNames;
  data: any;
}

export interface PlayerHitEventData {
  damage: number;
}

export const makePlayerHitEventData = (damage: number): PlayerHitEventData => ({
  damage: damage
});

export function findEventsByName(inputEvents: GameEvent[], name: string) {
  return inputEvents.filter((gameEvent) => gameEvent.name === name);
}

export function pushUniqueEvent(inputEvents: GameEvent[], event: GameEvent) {
  return findEventsByName(inputEvents, event.name).length > 0
    ? inputEvents
    : inputEvents.push(event);
}

export function makeGameEvent(name: GameEventNames, data: any = {}): GameEvent {
  return { name: name, data: data };
}

export function getEventData<T>(event: GameEvent): T {
  return event.data as T;
}

export function findFirstEventByName(inputEvents: GameEvent[], name: string) {
  return findEventsByName(inputEvents, name).length === 0 ?
    Nothing :
    Just(findEventsByName(inputEvents, name)[0]);
}

export interface ItemHitEventData {
  id: string;
}

export function makeItemHitEvent(id: string): GameEvent {
  return makeGameEvent(GameEventNames.ITEM_HIT, { id: id });
}

export interface EnemyDeadEventData {
  x: number;
  y: number;
  id: string;
}

export function makeEnemyDeadEvent(enemy: Character): GameEvent {
  return makeGameEvent(GameEventNames.ENEMY_DEAD, { x: enemy.x, y: enemy.y, id: enemy.id });
}

export interface WeaponReadyEventData {
  id: string;
}

export function makeWeaponReadyEvent(data: WeaponReadyEventData): GameEvent {
  return makeGameEvent(GameEventNames.WEAPON_READY, data);
}
