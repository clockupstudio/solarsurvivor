export enum InGameControls {
  LEFT,
  RIGHT,
  UP,
  DOWN,
  MENU,
  //SHOOT,
  IDLE,
  PAUSE,
  WEAPON_1,
  WEAPON_2,
}
