
export const NOT_FOUND_GUN: Gun = {
  id: "not_found",
  name: "Not Found",
  speed: 0,
  isReady: false,
  fireRate: 0,
};

export const findGunById = (guns: Gun[], id: string): Gun => {
  return guns.find((gun) => gun.id === id) ?? NOT_FOUND_GUN;
};

export function updateGunReady(gun: Gun, id: string): Gun {
  return {
    ...gun,
    isReady: gun.id === id,
  };
}

export interface WeaponSlots {
  primary: Gun;
  secondary: Gun;
}
export interface Gun {
  id: string;
  name: string;
  speed: number;
  isReady: boolean;
  fireRate: number;
}

export interface SelectedGun {
  weapon1Id: string;
  weapon2Id: string;
}

export function makeSelectedGun(weapon1Id: string, weapon2Id: string): SelectedGun {
  return { weapon1Id, weapon2Id };
}

