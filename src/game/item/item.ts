import { Maybe, Just } from "purify-ts";
import { GameState } from "../interfaces";
import { EnemyDeadEventData, findEventsByName, findFirstEventByName, GameEventNames, getEventData, ItemHitEventData } from "../events/event";
import { increasePlayerLife } from "../character/player";
import { IPlayerRecoverStarter } from "../states/play.state";
import { Character, makeCharacter } from "../character/character";
import { IRandom, IUniqueIdGenerator } from "../level/level";
import { DEFAULT_ITEM_CONFIG } from '../config/item.config';
import { processCoinHit } from "./coin";
import { shouldSpawnItem, spawnItems } from "./item.spawn";
import { ItemTypes } from "./item.types";

export interface Item extends Character {
    sourceId: string;
    type: ItemTypes;
}

export const makeItem = (character: Character, sourceId: string = '', type: ItemTypes=ItemTypes.health): Item => ({
    ...character,
    sourceId,
    type
});

export const updateItems = (context: IItemContext) => (gameState: GameState): Maybe<GameState> => {
    return Just(gameState)
        .chain(spawnItems(context)(shouldSpawnItem(context, gameState)))
        .chain(readItemHit);
};

export interface IItemContext extends IPlayerRecoverStarter, IRandom, IUniqueIdGenerator { }

const removeItemById = (itemId: string) => (gameState: GameState): Maybe<GameState> =>
    Just({
        ...gameState,
        items: gameState.items.filter(item => item.id !== itemId)
    });

const itemEffects: Record<ItemTypes, (state: GameState) => GameState> = {
    [ItemTypes.health]: increasePlayerLife,
    [ItemTypes.coin]: processCoinHit,
};
    
export const processItemHit = (id: string, gameState: GameState): Maybe<GameState> =>
    Just(gameState)
        .chain(state => {
            const item = state.items.find(item => item.id === id);
            return item 
                ? Just(itemEffects[item.type](state))
                : Just(state);
        })
        .chain(removeItemById(id));

export const readItemHit = (gameState: GameState): Maybe<GameState> =>
    findFirstEventByName(gameState.inputEvents, 'ITEM_HIT').caseOf({
        Just: (event) =>
            Just(event)
                .map(getEventData<ItemHitEventData>)
                .map(data => data.id)
                .chain(id => processItemHit(id, gameState)),
        Nothing: () => Just(gameState)
    });