import { Maybe, Just } from "purify-ts";
import { makeCharacter } from "../character/character";
import { DEFAULT_ITEM_CONFIG } from "../config/item.config";
import { EnemyDeadEventData, findEventsByName, GameEventNames, getEventData } from "../events/event";
import { GameState } from "../interfaces";
import { IItemContext, makeItem, Item } from "./item";
import { AnimationKeys } from "../character/animations";
import { ItemTypes } from "./item.types";

export const spawnItems = (context: IItemContext) => (shouldSpawnItem: (data: EnemyDeadEventData) => boolean) => (gameState: GameState): Maybe<GameState> => {
    const items = findEventsByName(gameState.inputEvents, GameEventNames.ENEMY_DEAD)
        .map(event => getEventData<EnemyDeadEventData>(event))
        .filter(shouldSpawnItem)
        .map(data => {
            const randomType = getRandomItemType(context);
            return makeItem(
                makeCharacter({
                    ...DEFAULT_ITEM_CONFIG,
                    x: data.x,
                    y: data.y,
                    id: context.newUUId(),
                    animationKey: randomType === ItemTypes.health ? AnimationKeys.ItemHealth : AnimationKeys.ItemCoin
                }), 
                data.id,
                randomType
            );
        });

    return Just({ ...gameState, items: [...gameState.items, ...items] });
};

const SPAWN_CONFIG = {
    baseSpawnRate: 30,
    itemChances: {
        [ItemTypes.health]: { min: 1, max: 20 },
        [ItemTypes.coin]: { min: 21, max: 100 }
    }
};

export const getRandomItemType = (context: IItemContext): ItemTypes => {
    const roll = context.between(1, 100);
    
    if (roll <= SPAWN_CONFIG.itemChances[ItemTypes.health].max) {
        return ItemTypes.health;
    }
    return ItemTypes.coin;
};

export const shouldSpawnItem = (context: IItemContext, gameState: GameState) => (data: EnemyDeadEventData): boolean =>
    isItemNotSpawnedFromExistingSource(gameState.items, data.id)
    && hasSpawnProbability(context);

const hasSpawnProbability = (context: IItemContext): boolean =>
    context.between(0, 100) <= SPAWN_CONFIG.baseSpawnRate;

export const isItemNotSpawnedFromExistingSource = (items: Item[], sourceId: string): boolean =>
    !items.some(item => item.sourceId === sourceId);