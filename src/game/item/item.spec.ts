import { IItemContext, makeItem, readItemHit, updateItems } from "./item";
import { GameState } from "../interfaces";
import { makeGameEvent, GameEventNames, makeEnemyDeadEvent, makeItemHitEvent } from "../events/event";
import { makeGameState } from "../states/gamestate";
import { makeCharacter } from "../character/character";
import { processItemHit } from './item';
import { ItemTypes } from "./item.types";
describe('Item', () => {
    describe('updateItems', () => {
        const updateTestCases = [
            {
                name: 'should process both item spawning and item hits',
                events: [
                    makeEnemyDeadEvent(makeCharacter({ id: 'enemy-1', x: 100, y: 200 })),
                    makeItemHitEvent('existing-id')
                ],
                existingItems: [makeItem(makeCharacter({ id: 'existing-id', x: 300, y: 400 }), 'existing-id')],
                randomValue: 0,
                expectedItemCount: 1,
                expectedItems: [{
                    sourceId: 'enemy-1'
                }]
            },
            {
                name: 'should only process hits when spawn chance fails',
                events: [
                    makeEnemyDeadEvent(makeCharacter({ id: 'enemy-1', x: 100, y: 200 })),
                    makeItemHitEvent('existing-id')
                ],
                existingItems: [makeItem(makeCharacter({ id: 'existing-id', x: 300, y: 400 }), 'existing-id')],
                randomValue: 100,
                expectedItemCount: 0
            }
        ];

        test.each(updateTestCases)('$name', ({ events, existingItems, randomValue, expectedItemCount, expectedItems }) => {
            // Arrange
            const context: IItemContext = {
                startRecoverTimer: jest.fn(),
                between: () => randomValue,
                newUUId: jest.fn()
            };

            const result = updateItems(context)(makeGameState({
                inputEvents: events,
                items: existingItems
            }));
            const resultState = result.extract();

            expect(resultState?.items).toHaveLength(expectedItemCount);
            if (expectedItems) {
                expectedItems.forEach((expectedItem, index) => {
                    expect(resultState?.items[index]).toEqual(
                        expect.objectContaining(expectedItem)
                    );
                });
            }
        });
    });

    describe('readItemHit', () => {
        interface TestCase {
            name: string;
            initialState: GameState;
            expectedLife: number;
            expectedItemsLength: number;
            expectedRemainingItemId?: string;
        }

        const testCases: TestCase[] = [
            {
                name: 'should increase player life and remove item when item hit event exists',
                initialState: makeGameState({
                    player: makeCharacter({ life: 1 }),
                    items: [
                        makeItem(makeCharacter({ id: "test-item-1" }), "test-item-1"),
                        makeItem(makeCharacter({ id: "test-item-2" }), "test-item-2")
                    ],
                    inputEvents: [
                        makeGameEvent(GameEventNames.ITEM_HIT, { id: "test-item-1" })
                    ]
                }),
                expectedLife: 2,
                expectedItemsLength: 1,
                expectedRemainingItemId: "test-item-2"
            },
            {
                name: 'should not modify state when no item hit event exists',
                initialState: makeGameState({
                    player: makeCharacter({ life: 1 }),
                    items: [
                        makeItem(makeCharacter({ id: "test-item-1" }), "test-item-1"),
                        makeItem(makeCharacter({ id: "test-item-2" }), "test-item-2")
                    ],
                    inputEvents: []
                }),
                expectedLife: 1,
                expectedItemsLength: 2
            }
        ];

        test.each(testCases)('$name', ({ initialState, expectedLife, expectedItemsLength, expectedRemainingItemId }) => {
            // Act
            const result = readItemHit(initialState);

            // Assert
            result.caseOf({
                Just: (state) => {
                    expect(state.player.life).toBe(expectedLife);
                    expect(state.items).toHaveLength(expectedItemsLength);
                    if (expectedRemainingItemId) {
                        expect(state.items[0].id).toBe(expectedRemainingItemId);
                    }
                },
                Nothing: () => fail('Expected Just, got Nothing')
            });
        });
    });

    describe('processItemHit', () => {
        const mockGameState: GameState = makeGameState({
            coins: 0,
            player: makeCharacter({ life: 3 }),
            items: [
                makeItem(makeCharacter({ id: 'health-1'}), 'source-1', ItemTypes.health),
                makeItem(makeCharacter({ id: 'coin-1'}), 'source-2', ItemTypes.coin)
            ],
        });

        it('should increase player life when hitting health item', () => {
            const result = processItemHit('health-1', mockGameState);
            
            expect(result?.extract()?.player.life).toBe(4);
            expect(result?.extract()?.items).not.toContain(
                expect.objectContaining({ id: 'health-1' })
            );
        });

        it('should increase player coins when hitting coin item', () => {
            const result = processItemHit('coin-1', mockGameState);
            
            expect(result?.extract()?.coins).toBe(1);
            expect(result?.extract()?.items).not.toContain(
                expect.objectContaining({ id: 'coin-1' })
            );
        });
    });
}); 
