import { AnimationKeys } from "../character/animations";
import { makeCharacter } from "../character/character";
import { makeGameEvent, GameEventNames } from "../events/event";
import { makeGameState } from "../states/gamestate";
import { makeItem, Item } from "./item";
import { spawnItems, isItemNotSpawnedFromExistingSource, getRandomItemType, shouldSpawnItem } from "./item.spawn";
import { ItemTypes } from "./item.types";

describe('Item spawn system', () => {
    describe('spawnItems', () => {
        const testCases = [
            {
                name: 'should not spawn item when no ENEMY_DEAD events exist',
                initialState: makeGameState({
                    inputEvents: [],
                    items: []
                }),
                spawnRoll: 1,    // Would spawn
                typeRoll: 21,    // Would be coin
                expectedItems: []
            },
            {
                name: 'should spawn health item when roll is for health',
                initialState: makeGameState({
                    inputEvents: [
                        makeGameEvent(GameEventNames.ENEMY_DEAD, {
                            id: 'enemy-1',
                            x: 100,
                            y: 200
                        })
                    ],
                    items: []
                }),
                spawnRoll: 1,    // Will spawn
                typeRoll: 1,     // Will be health (1-20)
                expectedItems: [
                    makeItem(makeCharacter({
                        x: 100,
                        y: 200,
                        speed: 0,
                        active: true,
                        offset: 12,
                        life: 1,
                        id: 'test-uuid',
                        animationKey: AnimationKeys.ItemHealth
                    }), 'enemy-1', ItemTypes.health)
                ]
            },
            {
                name: 'should spawn coin item when roll is for coin',
                initialState: makeGameState({
                    inputEvents: [
                        makeGameEvent(GameEventNames.ENEMY_DEAD, {
                            id: 'enemy-1',
                            x: 100,
                            y: 200
                        })
                    ],
                    items: []
                }),
                spawnRoll: 1,    // Will spawn
                typeRoll: 21,    // Will be coin (21-100)
                expectedItems: [
                    makeItem(makeCharacter({
                        x: 100,
                        y: 200,
                        speed: 0,
                        active: true,
                        offset: 12,
                        life: 1,
                        id: 'test-uuid',
                        animationKey: AnimationKeys.ItemCoin
                    }), 'enemy-1', ItemTypes.coin)
                ]
            },
            {
                name: 'should not spawn when probability check fails',
                initialState: makeGameState({
                    inputEvents: [
                        makeGameEvent(GameEventNames.ENEMY_DEAD, {
                            id: 'enemy-1',
                            x: 100,
                            y: 200
                        })
                    ],
                    items: []
                }),
                spawnRoll: 31,   // Won't spawn (> 30)
                typeRoll: 21,    // Would be coin, but won't matter
                expectedItems: []
            },
            {
                name: 'should preserve existing items when spawning new ones',
                initialState: makeGameState({
                    inputEvents: [
                        makeGameEvent(GameEventNames.ENEMY_DEAD, {
                            id: 'enemy-2',
                            x: 100,
                            y: 200
                        })
                    ],
                    items: [
                        makeItem(makeCharacter({
                            id: 'existing-item'
                        }), 'enemy-1', ItemTypes.health)
                    ]
                }),
                spawnRoll: 1,    // Will spawn
                typeRoll: 21,    // Will be coin
                expectedItems: [
                    makeItem(makeCharacter({
                        id: 'existing-item'
                    }), 'enemy-1', ItemTypes.health),
                    makeItem(makeCharacter({
                        x: 100,
                        y: 200,
                        speed: 0,
                        active: true,
                        offset: 12,
                        life: 1,
                        id: 'test-uuid',
                        animationKey: AnimationKeys.ItemCoin
                    }), 'enemy-2', ItemTypes.coin)
                ]
            }
        ];

        testCases.forEach((testCase) => {
            it(testCase.name, () => {
                // Arrange
                let rollCount = 0;
                const mockContext = {
                    newUUId: jest.fn().mockReturnValue('test-uuid'),
                    startRecoverTimer: jest.fn(),
                    between: jest.fn().mockImplementation(() => {
                        // First roll is for spawn check, second is for item type
                        return rollCount++ === 0 ? testCase.spawnRoll : testCase.typeRoll;
                    })
                };

                // Act
                const result = spawnItems(mockContext)(
                    (data) => shouldSpawnItem(mockContext, testCase.initialState)(data)
                )(testCase.initialState);

                // Assert
                expect(result.extract()?.items).toEqual(testCase.expectedItems);
            });
        });

        it('should generate unique IDs for each item with different types', () => {
            // Arrange
            const mockUUIDs = ['uuid-1', 'uuid-2'];
            const mockContextWithMultipleIDs = {
                newUUId: jest.fn()
                    .mockReturnValueOnce(mockUUIDs[0])
                    .mockReturnValueOnce(mockUUIDs[1]),
                startRecoverTimer: jest.fn(),
                between: jest.fn()
                    .mockReturnValueOnce(0)
                    .mockReturnValueOnce(1)
            };

            const initialState = makeGameState({
                inputEvents: [
                    makeGameEvent(GameEventNames.ENEMY_DEAD, {
                        id: 'enemy-1',
                        x: 100,
                        y: 200
                    }),
                    makeGameEvent(GameEventNames.ENEMY_DEAD, {
                        id: 'enemy-2',
                        x: 300,
                        y: 400
                    })
                ],
                items: []
            });

            // Act
            const result = spawnItems(mockContextWithMultipleIDs)(() => true)(initialState);
            const items = result.extract()?.items;

            // Assert
            expect(mockContextWithMultipleIDs.newUUId).toHaveBeenCalledTimes(2);
            expect(items?.[0].id).toBe('uuid-1');
            expect(items?.[1].id).toBe('uuid-2');
        });
    });

    describe('isItemNotSpawnedFromExistingSource', () => {
        const testCases: Array<{
            description: string,
            items: Item[],
            sourceId: string,
            expected: boolean
        }> = [
                {
                    description: 'should return true when items array is empty',
                    items: [],
                    sourceId: 'source1',
                    expected: true
                },
                {
                    description: 'should return true when sourceId does not exist in items',
                    items: [
                        makeItem(makeCharacter({ id: 'item1' }), 'source1'),
                        makeItem(makeCharacter({ id: 'item2' }), 'source2')
                    ],
                    sourceId: 'source3',
                    expected: true
                },
                {
                    description: 'should return false when sourceId exists in items',
                    items: [
                        makeItem(makeCharacter({ id: 'item1' }), 'source1'),
                        makeItem(makeCharacter({ id: 'item2' }), 'source2')
                    ],
                    sourceId: 'source1',
                    expected: false
                },
                {
                    description: 'should return false when sourceId exists multiple times in items',
                    items: [
                        makeItem(makeCharacter({ id: 'item1' }), 'source1'),
                        makeItem(makeCharacter({ id: 'item2' }), 'source1')
                    ],
                    sourceId: 'source1',
                    expected: false
                }
            ];

        test.each(testCases)('$description', ({ items, sourceId, expected }) => {
            const result = isItemNotSpawnedFromExistingSource(items, sourceId);
            expect(result).toBe(expected);
        });
    });

    describe('getRandomItemType', () => {
        const testCases = [
            { roll: 1, expected: ItemTypes.health },   // 1-20: health
            { roll: 20, expected: ItemTypes.health },  // 1-20: health
                { roll: 21, expected: ItemTypes.coin },    // 21-100: coin
                { roll: 100, expected: ItemTypes.coin },   // 21-100: coin
            ];

            testCases.forEach(({ roll, expected }) => {
                it(`should return ${expected} when roll is ${roll}`, () => {
                    const context = {
                        between: () => roll,
                        newUUId: jest.fn(),
                    startRecoverTimer: jest.fn()
                };
                expect(getRandomItemType(context)).toBe(expected);
            });
        });
    });

    describe('shouldSpawnItem', () => {
        const testCases = [
            { 
                roll: 1, 
                existingItems: [], 
                sourceId: '1',
                expected: true,
                description: 'should spawn when roll is 1 and no existing items'
            },
            { 
                roll: 30, 
                existingItems: [], 
                sourceId: '1',
                expected: true,
                description: 'should spawn when roll is 30 and no existing items'
            },
            { 
                roll: 31, 
                existingItems: [], 
                sourceId: '1',
                expected: false,
                description: 'should not spawn when roll is 31'
            },
            { 
                roll: 100, 
                existingItems: [], 
                sourceId: '1',
                expected: false,
                description: 'should not spawn when roll is 100'
            },
            { 
                roll: 1, 
                existingItems: [makeItem(makeCharacter({ id: 'item1' }), 'source1')], 
                sourceId: 'source1',
                expected: false,
                description: 'should not spawn when item already exists from source'
            }
        ];

        testCases.forEach(({ roll, existingItems, sourceId, expected, description }) => {
            it(description, () => {
                const context = {
                    between: () => roll,
                    newUUId: jest.fn(),
                    startRecoverTimer: jest.fn()
                };
                const gameState = {
                    ...makeGameState(),
                    items: existingItems
                };
                const data = { id: sourceId, x: 0, y: 0 };
                
                expect(shouldSpawnItem(context, gameState)(data)).toBe(expected);
            });
        });
    });
});