import { GameState } from "../interfaces";

export const processCoinHit = (gameState: GameState): GameState =>
    ({...gameState, coins: gameState.coins + 1});
