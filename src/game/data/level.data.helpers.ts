import { NextLevelData } from "./level.data";

export function makeTestLevelData(nextLevel: NextLevelData = { condition: 10, nextLevel: 2,}) {
    return {
        level: 1, min: 0, max: 1,
        enemies: [{ index: 1, chance: 100 }],
        spawnPoints: [{ x: 0, y: 0 }, { x: 144, y: 0 }, { x: 0, y: 160 }, { x: 144, y: 160 },],
        nextLevel: nextLevel,
    };
}