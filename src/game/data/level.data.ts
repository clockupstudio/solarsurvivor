import { Maybe } from "purify-ts/Maybe";
import { Vector2 } from "../character/character"
import { IRandom } from "../level/level";
import { levels } from "./levels";

export interface LevelData {
    level: number,
    min: number,
    max: number,
    enemies: EnemyLevelData[],
    spawnPoints: Vector2[],
    nextLevel: NextLevelData,
}

export interface EnemyLevelData {
    index: number,
    chance: number,
}

export interface NextLevelData {
    condition: number,
    nextLevel: number,
}

export function getSpawnPoint(levelData: LevelData, random: IRandom): Vector2 {
    return levelData.spawnPoints[random.between(0, levelData.spawnPoints.length)];
}

export function getLevelDataByIndex(levelIndex: number): Maybe<LevelData> {
    return Maybe.fromNullable(levels.find((levelData)=>levelData.level == levelIndex))
}