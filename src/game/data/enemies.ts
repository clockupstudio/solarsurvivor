import { AnimationKeys } from "../character/animations";
import { EnemyData } from "./enemy.data";

export const enemies: EnemyData[] = [
    {  index: 1, speed: 0.2, animationKey: AnimationKeys.EnemyIdle, life: 1},
    {  index: 2, speed: 0.2, animationKey: AnimationKeys.Enemy2Idle, life: 1 },
]