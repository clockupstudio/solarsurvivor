import { LevelData } from "./level.data";

export const levels: LevelData[] = [
    {
        level: 1, min: 0, max: 1,
        enemies: [{ index: 1, chance: 100 }],
        spawnPoints: [{ x: 0, y: 0 }, { x: 144, y: 0 }, { x: 0, y: 160 }, { x: 144, y: 160 },],
        nextLevel: { condition: 10, nextLevel: 2,},
    },
    {
        level: 2, min: 0, max: 10,
        enemies: [{ index: 1, chance: 100 }, { index: 2, chance: 100 },],
        spawnPoints: [{ x: 0, y: 0 }, { x: 144, y: 0 }, { x: 0, y: 160 }, { x: 144, y: 160 },],
        nextLevel: { condition: 0, nextLevel: 2,},
    },
];