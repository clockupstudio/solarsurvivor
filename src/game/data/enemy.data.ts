import { AnimationKeys } from "../character/animations";

export interface EnemyData {
    index: number, 
    speed: number, 
    animationKey: AnimationKeys, 
    life: number,
};