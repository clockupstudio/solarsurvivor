export const guns = [
  {
    id: "0",
    name: "basic1",
    speed: 5,
    isReady: true,
    fireRate: 1000,
  },
  {
    id: "1",
    name: "basic2",
    speed: 5,
    isReady: true,
    fireRate: 50,
  },
];
