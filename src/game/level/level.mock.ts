export const makeMockSpawnEnemiesContext = (randomOerder: number[], uuid = '') =>({
    between: jest.fn().mockImplementation(()=>randomOerder.pop() ?? 0),
    newUUId: jest.fn().mockReturnValue(uuid),
});