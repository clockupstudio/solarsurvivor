import { Maybe, Nothing } from "purify-ts";
import { makeCharacter } from "../character/character";
import { enemies } from "../data/enemies";
import { GameEventNames, makeGameEvent } from "../events/event";
import { makeGameState } from "../states/gamestate";
import { randomPickEnemyByChance, spawnEnemies } from "./level";
import { makeMockSpawnEnemiesContext } from "./level.mock";
import { AnimationKeys } from "../character/animations";

// Add mock for spawnEnemy
jest.mock('../enemy/enemy.spawn', () => ({
  ...jest.requireActual('../enemy/enemy.spawn'),
  spawnEnemy: (gameState: any, enemyData: any) => ({
    ...enemyData,
    x: 0,
    y: 0
  })
}));

describe('spawnEnemies', () => {
    // (Type, number of enemies) -> list of Character
    // (GameState{}) -> GameState{ }
    // (GameState{ inputEvent:[SPAWN_ENEMY], Random(0) }) -> GameState{ }
    // (GameState{ inputEvent:[SPAWN_ENEMY], Random(1) }) -> GameState { enemies: [ Character() ] }
    // (GameState{ inputEvent:[SPAWN_ENEMY], Random(2) }) -> GameState { [ Character(), Character() ] }
    const makeReadyEnemy = () => makeCharacter({ ...enemies[0], x: 0, active: true });

    [
        {
            case: 'Return empty list when no event',
            gameState: makeGameState(),
            random: [0],
            expected: makeGameState(),
        },
        {
            case: 'Return empty list when event but given 0 from a randomization',
            gameState: makeGameState({ inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)] }),
            random: [0],
            expected: makeGameState({ inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)] }),
        },
        {
            case: 'Return 1 character in the list when event but given 1 from a randomization',
            gameState: makeGameState({ inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)] }),
            random: [0, 1,],
            expected: makeGameState({
                enemies: [makeReadyEnemy()],
                inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)]
            }),
        },
        {
            case: 'Return 2 characters in the list when event but given 2 from a randomization',
            gameState: makeGameState({ inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)] }),
            random: [ 0, 2],
            expected: makeGameState({
                enemies: [makeReadyEnemy(), makeReadyEnemy()],
                inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)]
            }),
        },
        {
            case: 'Return 2 characters in the list when 1 alreaddy exist and given 1 from a randomization',
            gameState: makeGameState({
                enemies: [makeCharacter()],
                inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)]
            }),
            random: [ 0, 1],
            expected: makeGameState({
                enemies: [makeCharacter(), makeReadyEnemy()],
                inputEvents: [makeGameEvent(GameEventNames.ENEMY_SPAWN)]
            }),
        },
    ].forEach((testCase) => {
        it(testCase.case, () => {
            let context = makeMockSpawnEnemiesContext(testCase.random);
            expect(spawnEnemies(context)(testCase.gameState).extract()).toEqual(testCase.expected)
        });
    });
});

describe('randomPickEnemyByChance', () => {
    [
        {
            name: 'return nothing if both level and enemy data are empty',
            randomResult: 0,
            listOfEnemy: [],
            listOfEnemyData: [],
            expected: Nothing,
        },
        {
            name: 'return single enemy if only 1 in the list of enemy data',
            randomResult: 0,
            listOfEnemy: [makeTestEnemyData(1)],
            listOfEnemyData: [{ index: 1, chance: 100 }],
            expected: Maybe.of(makeTestEnemyData(1)),
        },
        {
            name: 'return first enemy if random falls into the first',
            randomResult: 0,
            listOfEnemy: [makeTestEnemyData(1)],
            listOfEnemyData: [{ index: 1, chance: 90 }, { index: 2, chance: 10 }],
            expected: Maybe.of(makeTestEnemyData(1)),
        },
        {
            name: 'return first enemy if random falls into the first',
            randomResult: 99,
            listOfEnemy: [makeTestEnemyData(2)],
            listOfEnemyData: [{ index: 1, chance: 90 }, { index: 2, chance: 10 }],
            expected: Maybe.of(makeTestEnemyData(2)),
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            const mockRandom = { between: jest.fn().mockReturnValue(testCase.randomResult) };

            let result = randomPickEnemyByChance(mockRandom)
                (testCase.listOfEnemy)
                (testCase.listOfEnemyData);

            expect(result).toEqual(testCase.expected);
        });
    })

});

function makeTestEnemyData(index: number) {
    return {
        index: index,
        speed: 1,
        animationKey: AnimationKeys.EnemyIdle,
        life: 1,
    }
};