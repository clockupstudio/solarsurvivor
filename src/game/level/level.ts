import { times } from "lodash";
import { Character, makeCharacter, Vector2, } from "../character/character";
import { enemies } from "../data/enemies";
import { v4 as uuidv4 } from "uuid";
import { GameState } from "../interfaces";
import { Just, Maybe, Nothing, number } from "purify-ts";
import { findEventsByName, findFirstEventByName, GameEvent, GameEventNames, makeGameEvent } from "../events/event";
import { levels } from "../data/levels";
import { EnemyLevelData, getSpawnPoint, LevelData } from "../data/level.data";
import { EnemyData } from "../data/enemy.data";
import { spawnEnemy } from "../enemy/enemy.spawn";

// // get how many enemmies want to be spawned
// (randomm: IRandom)(min: number, max: number): number

// // get types of enemies
// (howMany: number): number[]

// // generate positions by the number of the enemies
// (enemyTypes: number[], randomizedPositions: Vector2[]): { enemyType: number, position: Vector2 }[]

// // generate list of enemy character
// (radomizedData: { enemyType: number, position: Vector2 }[]): Character[]

export interface ISpawnEnemiesContext extends IRandom, IUniqueIdGenerator { }

export interface IUniqueIdGenerator {
  newUUId(): string
}

export interface IRandom {
  between(min: number, max: number): number;
}

export const spawnEnemies = (context: ISpawnEnemiesContext) => (gameState: GameState) => {
  return findFirstEventByName(gameState.inputEvents, GameEventNames.ENEMY_SPAWN)
    .caseOf({
      Nothing: () => Just([]),
      Just: () => getCurrentLevelData(gameState.level)
        .chain((levelData) => {
          return Just(times(
            randomEnemiesNumber(context)(levelData).orDefault(0),
            () => makeReadyEnemy(context)(levelData)
              .map(enemy => spawnEnemy(gameState, enemy))
              .extract()
          ));
        })
    })
    .chain((enemiesData) => Just({
      ...gameState,
      enemies: gameState.enemies.concat(enemiesData.map((e) => makeCharacter(e)))
    }));
}

export const getCurrentLevelData = (level: number) => Maybe.fromNullable(levels.find((gameLevel) => gameLevel.level === level));

export const randomEnemiesNumber = (random: IRandom) => (levelEnemiesRange: { min: number, max: number }) => Just(random.between(levelEnemiesRange.min, levelEnemiesRange.max));

export const makeReadyEnemy = (context: ISpawnEnemiesContext) => (levelData: LevelData) => {
  return Maybe.of(makeCharacter())
    .chain((character) => Just({ ...character, id: context.newUUId() }))
    .chain((character) => Just({ ...character, active: true }))
    .chain((character) => randomPickEnemyByChance(context)(enemies)(levelData.enemies)
      .chain((enemyData) => Just({ ...character, ...enemyData })))
}

// randomPickEnemyByChance
// Random, list of Enemies, EnemyLevelData -> Enemy
// (() -> 0, [], []) -> Nothing
// (() -> 0, [ EnemyData{ index: 1 } ], [{ index: 1, chance: 100 }]) -> EnemyData{ index: 1 }
// (() -> 0, [ EnemyData{ index: 1 }, EnemyData{ index: 2 } ], [{ index: 1, chance: 10 },{ index: 2, chance: 90 }]) 
//    -> EnemyData{ index: 1 }

export const randomPickEnemyByChance = (randomm: IRandom) => (enemies: EnemyData[]) => (enemyLevelData: EnemyLevelData[]): Maybe<EnemyData> => {
  const totalChance = enemyLevelData.reduce((acc: number, n: EnemyLevelData) => { return acc += n.chance }, 0);
  const randomNumber = randomm.between(0, totalChance);
  const pickedEnemyData = enemyLevelData.find((levelData) => {
    const cumulativeChance = levelData.chance + enemyLevelData.slice(0, enemyLevelData.indexOf(levelData)).reduce(
      (acc, prevEnemy) => acc + prevEnemy.chance,
      0
    );
    return randomNumber < cumulativeChance;
  });

  return Maybe.fromNullable(enemies.find((data) => data.index === pickedEnemyData?.index));
}