import { GameCamera } from "./camera/camera";
import { Gun, SelectedGun } from "./weapon/gun";
import { Character } from "./character/character";
import { InGameControls } from "./controls";
import { GameEvent } from "./events/event";
import { Item } from "./item/item";
import { GameStates } from "./states/gamestate";
import { IPlayStateContext } from "./states/play.state";
import { WeaponSlots } from "./weapon/gun";

export interface ICheckable {
  isReady: boolean;
}

export interface ITriggerable {
  start(): void;
}

export interface IExitable {
  exit(result: SelectedGun): void;
}

export interface IGameScene
  extends
  IGameSceneRenderer,
  IPauseSceneOpener,
  IGameOverSceneOpener,
  IGameSceneCamera,
  IPlayStateContext {
  readInput(): InGameControls[];
  clearInputEvents(): void;
}

export interface IEventFinnder {
  findEventsByName(name: string): GameEvent[];
}

export interface IGameSceneRenderer {
  render(state: GameState): void;
}

export interface IPauseSceneOpener {
  enterPauseScene(state: GameState): void;
}

export interface IGameOverSceneOpener {
  enterGameOverScene(): void
}

export interface IGameSceneCamera {
  cameras: {
    main: {
      scrollX: number;
      scrollY: number;
    }
  }
}

export interface GameState {
  player: Character;
  weaponSlots: WeaponSlots;
  guns: Gun[];
  bullets: Character[];
  enemies: Character[];
  camera: GameCamera;
  state: GameStates;
  controls: InGameControls[];
  inputEvents: GameEvent[];
  level: number;
  killsCount: number;
  items: Item[];
  coins: number;
}

export interface Bounds {
  left: number;
  right: number;
  top: number;
  bottom: number;
}


