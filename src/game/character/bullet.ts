import { Character, Directions, Vector2 } from "./character";
import { InGameControls } from "../controls";
import { Gun } from "../weapon/gun";

export function makeGun({
  id = '',
  speed = 1,
  isReady = true,
  fireRate = 500,
  name = ''
}: Partial<Gun> = {}): Gun {
  return {
    id: id,
    name: name,
    speed: speed,
    isReady: isReady,
    fireRate: fireRate,
  };
}

export function activeBullet(
  bullet: Character,
  player: Character,
  controls: InGameControls[]
): Character {
  return bullet.active
    ? bullet
    : {
        ...(controls.includes(InGameControls.WEAPON_1)
          ? setBulletDirection(initBulletPosition(bullet, player), player)
          : bullet),
        active: bullet.active ? true : controls.includes(InGameControls.WEAPON_1),
      };
}

// Could create a new function to handle direction-based position offsets
function getOffsetByDirection(position: Vector2, direction: Vector2, offset: number): Vector2 {
  return {
      x: position.x + (direction.x * offset),
      y: position.y + (direction.y * offset)
  };
}

// Then simplify initBulletPosition to:
export function initBulletPosition(bullet: Character, player: Character): Character {
  const position = getOffsetByDirection(
      { x: player.x, y: player.y }, 
      player.shootingDirection, 
      player.offset
  );
  return { ...bullet, ...position };
}

export function setBulletDirection(
  bullet: Character,
  player: Character
): Character {
  return { 
    ...bullet, 
    direction: player.shootingDirection 
  };
}

export function setBulletSpeed(bullet: Character, gun: Gun): Character {
  return { ...bullet, speed: gun.speed };
}

export function moveBullet(bullet: Character): Character {
  let x = bullet.x;
  if (bullet.direction === Directions.LEFT) {
    x = bullet.x - bullet.speed;
  } else if (bullet.direction === Directions.RIGHT) {
    x = bullet.x + bullet.speed;
  }
  let y = bullet.y;
  if (bullet.direction === Directions.UP) {
    y = bullet.y - bullet.speed;
  } else if (bullet.direction === Directions.DOWN) {
    y = bullet.y + bullet.speed;
  }
  return { ...bullet, x: x, y: y };
}