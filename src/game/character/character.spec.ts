import { Just } from "purify-ts/Maybe";
import { makeGameCamera } from "../camera/camera";
import {
  clampToBounds,
  Directions,
  makeCharacter,
  moveCharacter,
  updateDirection,
} from "./character";
import { InGameControls } from "../controls";
import { makeGameState } from "../states/gamestate";

describe("characterMovement", () => {
  [
    {
      case: "Empty controls should return same object",
      character: makeCharacter(),
      controls: [],
      expected: makeCharacter(),
    },
    {
      case: "Should move to the left when given only LEFT",
      character: makeCharacter(),
      controls: [InGameControls.LEFT],
      expected: makeCharacter({ x: -1 }),
    },
    {
      case: "Should move to the left when given only RIGHT",
      character: makeCharacter(),
      controls: [InGameControls.RIGHT],
      expected: makeCharacter({ x: 1 }),
    },
    {
      case: "Should move upward when given only UP",
      character: makeCharacter(),
      controls: [InGameControls.UP],
      expected: makeCharacter({ x: 0, y: -1 }),
    },
    {
      case: "Should move downward when given only DOWN",
      character: makeCharacter(),
      controls: [InGameControls.DOWN],
      expected: makeCharacter({ x: 0, y: 1 }),
    },
    {
      case: "Should move up-left when given LEFT and UP",
      character: makeCharacter(),
      controls: [InGameControls.LEFT, InGameControls.UP],
      expected: makeCharacter({ x: -1, y: -1 }),
    },
  ].forEach((testCase) => {
    it(testCase.case, () => {
      expect(moveCharacter(testCase.character, testCase.controls)).toEqual(
        testCase.expected
      );
    });
  });
});

describe("clampToBounds", () => {
  const testBounds = {
    left: 0,
    right: 800,
    top: 0,
    bottom: 600
  };

  [
    {
      case: "should not clamp when within bounds",
      character: makeCharacter({ x: 400, y: 300, offset: 10 }),
      expected: makeCharacter({ x: 400, y: 300, offset: 10 })
    },
    {
      case: "should clamp to left boundary",
      character: makeCharacter({ x: -5, y: 300, offset: 10 }),
      expected: makeCharacter({ x: 0, y: 300, offset: 10 })
    },
    {
      case: "should clamp to right boundary considering offset",
      character: makeCharacter({ x: 795, y: 300, offset: 10 }),
      expected: makeCharacter({ x: 790, y: 300, offset: 10 })
    },
    {
      case: "should clamp to top boundary",
      character: makeCharacter({ x: 400, y: -5, offset: 10 }),
      expected: makeCharacter({ x: 400, y: 0, offset: 10 })
    },
    {
      case: "should clamp to bottom boundary considering offset",
      character: makeCharacter({ x: 400, y: 595, offset: 10 }),
      expected: makeCharacter({ x: 400, y: 590, offset: 10 })
    },
    {
      case: "should clamp both x and y when out of bounds",
      character: makeCharacter({ x: -5, y: 605, offset: 10 }),
      expected: makeCharacter({ x: 0, y: 590, offset: 10 })
    }
  ].forEach((testCase) => {
    it(testCase.case, () => {
      expect(clampToBounds(testCase.character, testBounds))
        .toEqual(testCase.expected);
    });
  });
});

describe("updateDirection", () => {
  it("No change when empty controls", () => {
    expect(updateDirection(makeCharacter({}), [])).toEqual(makeCharacter({}));
  });

  it("Set direction to LEFT when input contains LEFT", () => {
    expect(updateDirection(makeCharacter({}), [InGameControls.LEFT])).toEqual(
      makeCharacter({ direction: Directions.LEFT })
    );
  });

  it("Set direction to RIGHT when input contains RIGHT", () => {
    expect(updateDirection(makeCharacter({}), [InGameControls.RIGHT])).toEqual(
      makeCharacter({ direction: Directions.RIGHT })
    );
  });

  it("Set direction to UP when input contains UP", () => {
    expect(updateDirection(makeCharacter({}), [InGameControls.UP])).toEqual(
      makeCharacter({ direction: Directions.UP })
    );
  });

  it("Set direction to UP when input contains DOWN", () => {
    expect(updateDirection(makeCharacter({}), [InGameControls.DOWN])).toEqual(
      makeCharacter({ direction: Directions.DOWN })
    );
  });
});

describe("movement with bounds", () => {
  const testBounds = {
    left: 0,
    right: 800,
    top: 0,
    bottom: 600
  };

  [
    {
      case: "should move and stay within bounds",
      initial: makeGameState({
        player: makeCharacter({ x: 5, y: 5, speed: 2, offset: 10 }),
        controls: [InGameControls.RIGHT, InGameControls.DOWN]
      }),
      expected: makeCharacter({ x: 7, y: 7, speed: 2, offset: 10 })
    },
    {
      case: "should stop at boundary when trying to move beyond",
      initial: makeGameState({
        player: makeCharacter({ x: 1, y: 1, speed: 5, offset: 10 }),
        controls: [InGameControls.LEFT, InGameControls.UP]
      }),
      expected: makeCharacter({ x: 0, y: 0, speed: 5, offset: 10 })
    }
  ].forEach((testCase) => {
    it(testCase.case, () => {
      const result = Just(testCase.initial)
        .map(state => ({
          ...state,
          player: moveCharacter(state.player, state.controls)
        }))
        .map(state => ({
          ...state,
          player: clampToBounds(state.player, testBounds)
        }))
        .extract();

      expect(result?.player).toEqual(testCase.expected);
    });
  });
});
