import { Just } from "purify-ts";
import { makeGameCamera } from "../camera/camera";
import {
  activeBullet,
  initBulletPosition,
  makeGun,
  moveBullet,
  setBulletDirection,
  setBulletSpeed,
} from "../character/bullet";
import { Directions, makeCharacter } from "../character/character";
import { InGameControls } from "../controls";
import { makeGameState } from "../states/gamestate";
import { destroyBullet, firePrimaryWeapon } from "../states/play.state";
import { AnimationKeys } from "./animations";

describe("activeBullet", () => {
  // Bullet, Controls -> Bullet
  // Bullet(active: false), [] -> Bullet(active: false)
  // Bullet(active: false), [Controls.SHOOT] -> Bullet(active: true)
  // Bullet(active: false), [Controls.SHOOT, Controls.UP] -> Bullet(active: true)
  // Bullet(active: false), [Controls.UP] -> Bullet(active: false)
  // [ { input: { bullet: makeBullet(active: false), controls: []}, expected: makeBullet(active: false)}].forEach(testCase=>{
  //     expect(activeBullet(testCase.input.bullet, testCase.input.controls)).toEqual(testCase.expected);
  //});
  [
    {
      it: "Should not activate when controls empty",
      bullet: {},
      controls: [],
      expected: { active: false },
    },
    {
      it: "Should activate when controls have WEAPON_1",
      bullet: {},
      controls: [InGameControls.WEAPON_1],
      expected: { active: true },
    },
    {
      it: "Should not activate when controls not contains WEAPON_1",
      bullet: {},
      controls: [InGameControls.UP],
      expected: { active: false },
    },
    {
      it: "should ignore controls after activated",
      bullet: { active: true },
      controls: [],
      expected: { active: true },
    },
    {
      it: "should set init position with player's position when WEAPON_1",
      bullet: {},
      player: { x: 10, y: 5 },
      controls: [InGameControls.WEAPON_1],
      expected: { active: true, x: 10, y: 5 },
    },
    {
      it: "should not set init position with player's position when controls is empty",
      bullet: { active: true },
      player: { x: 10, y: 5 },
      controls: [],
      expected: { active: true },
    },
    {
      it: "should set direction with player's direction when WEAPON_1",
      bullet: {},
      player: { shootingDirection: Directions.RIGHT },
      controls: [InGameControls.WEAPON_1],
      expected: { active: true, direction: Directions.RIGHT },
    },
    {
      it: "should not set direction when controls does not contains WEAPON_1",
      bullet: { direction: Directions.LEFT },
      player: { shootingDirection: Directions.RIGHT },
      controls: [],
      expected: { direction: Directions.LEFT },
    },
  ].forEach((testCase) =>
    it(testCase.it, () => {
      expect(
        activeBullet(
          makeCharacter(testCase.bullet),
          makeCharacter(testCase.player),
          testCase.controls
        )
      ).toEqual(makeCharacter(testCase.expected));
    })
  );
});

describe("initBulletPosition", () => {
  const testCases = [
    {
      name: "should spawn bullet based on shooting direction (LEFT) while moving RIGHT",
      player: makeCharacter({ 
        x: 0, 
        y: 0, 
        direction: Directions.RIGHT,
        shootingDirection: Directions.LEFT,
        offset: 10 
      }),
      expected: { x: -10, y: 0 }
    },
    {
      name: "should spawn bullet based on shooting direction (UP) while moving DOWN",
      player: makeCharacter({ 
        x: 0, 
        y: 0, 
        direction: Directions.DOWN,
        shootingDirection: Directions.UP,
        offset: 10 
      }),
      expected: { x: 0, y: -10 }
    }
  ];

  testCases.forEach(({ name, player, expected }) => {
    it(name, () => {
      expect(initBulletPosition(makeCharacter(), player))
        .toEqual(makeCharacter(expected));
    });
  });
});

// setBulletDirection
describe("setBulletDirection", () => {
  const testCases = [
    {
      name: "should set bullet direction to player's shooting direction (LEFT)",
      player: makeCharacter({ 
        direction: Directions.RIGHT,
        shootingDirection: Directions.LEFT 
      }),
      expected: Directions.LEFT
    },
    {
      name: "should set bullet direction to player's shooting direction (RIGHT)",
      player: makeCharacter({ 
        direction: Directions.LEFT,
        shootingDirection: Directions.RIGHT 
      }),
      expected: Directions.RIGHT
    },
    {
      name: "should set bullet direction to player's shooting direction (UP)",
      player: makeCharacter({ 
        direction: Directions.DOWN,
        shootingDirection: Directions.UP 
      }),
      expected: Directions.UP
    }
  ];

  testCases.forEach(({ name, player, expected }) => {
    it(name, () => {
      const bullet = makeCharacter();
      const result = setBulletDirection(bullet, player);
      expect(result.direction).toEqual(expected);
    });
  });
});

// setBulletSpeed()
// Bullet, Gun -> Bullet
// Bullet(), Gun(speed: 100) -> Bullet(speed: 100)
describe("setBulletSpeed", () => {
  it("set bullet speed from the gun", () => {
    expect(
      setBulletSpeed(makeCharacter(), makeGun({ speed: 10 }))
    ).toEqual(makeCharacter({ speed: 10 }));
  });
});

// moveBullet
// Bullet -> Bullet
// Bullet(x:0, y:0, speed: 1, direction: LEFT) -> Bullet(x:-1, y:0, speed: 1, direction: LEFT)
// Bullet(x:0, y:0, speed: 1, direction: RIGHT) -> Bullet(x:1, y:0, speed: 1, direction: RIGHT)
// Bullet(x:0, y:-1, speed: 1, direction: UP) -> Bullet(x:0, y:-1, speed: 1, direction: UP)
// Bullet(x:0, y:1, speed: 1, direction: DOWN) -> Bullet(x:0, y:1, speed: 1, direction: DOWN)
describe("moveBullet", () => {
  [
    {
      it: "move bullet to the left when direction is LEFT",
      input: { x: 0, y: 0, speed: 1, direction: Directions.LEFT },
      expect: { x: -1, y: 0, speed: 1, direction: Directions.LEFT },
    },
    {
      it: "move bullet to the right when direction is RIGHT",
      input: { x: 0, y: 0, speed: 1, direction: Directions.RIGHT },
      expect: { x: 1, y: 0, speed: 1, direction: Directions.RIGHT },
    },
    {
      it: "move bullet to the top when direction is UP",
      input: { x: 0, y: 0, speed: 1, direction: Directions.UP },
      expect: { x: 0, y: -1, speed: 1, direction: Directions.UP },
    },
    {
      it: "move bullet to the bottom when direction is DOWN",
      input: { x: 0, y: 0, speed: 1, direction: Directions.DOWN },
      expect: { x: 0, y: 1, speed: 1, direction: Directions.DOWN },
    },
  ].forEach((testCase) =>
    it(testCase.it, () => {
      expect(moveBullet(makeCharacter(testCase.input))).toEqual(
        makeCharacter(testCase.expect)
      );
    })
  );
});

describe("shooting bullet", () => {

  let recoverTimer = { startRecoverTimer: jest.fn() };

  // shoot
  // add bullet to the screen
  // GameState, list of Control -> GameState
  // GameState(bullet:[]), [] -> GameState(bullet:[])
  // GameState(bullet:[]), [SHOOT] -> GameState(bullet:[Bullet()])
  // GameState(bullet:[Bullet()]), [SHOOT] -> GameState(bullet:[Bullet(), Bullet()])
  it("Should not add bullet when given empty controls", () => {
    let nextState = firePrimaryWeapon(recoverTimer)(makeGameState({ controls: [] })).extract();
    expect(nextState).toEqual(makeGameState());
  });

  it("Should add bullet when controls contains SHOOT and disable gun", () => {
    let nextState = firePrimaryWeapon(recoverTimer)(makeGameState({
      weaponSlots: {
        primary: makeGun({ isReady: true }),
        secondary: makeGun({ isReady: false }),
      },
      controls: [
        InGameControls.WEAPON_1,
      ]
    })).extract();
    expect(nextState).toEqual(
      makeGameState({
        bullets: [makeCharacter({ active: true, animationKey: AnimationKeys.BulletIdle })],
        weaponSlots: {
          primary: makeGun({ isReady: false }),
          secondary: makeGun({ isReady: false }),
        },
        controls: [InGameControls.WEAPON_1,]
      })
    );
  });

  it("should not add bullet when weapon is not ready", () => {
    let nextState = firePrimaryWeapon(recoverTimer)(makeGameState({
      weaponSlots: {
        primary: makeGun({ isReady: false }),
        secondary: makeGun({ isReady: false }),
      },
      controls: [InGameControls.WEAPON_1,]
    })).extract();
    expect(nextState).toEqual(
      makeGameState({ weaponSlots: {
        primary: makeGun({ isReady: false }),
        secondary: makeGun({ isReady: false }),
      }, bullets: [], controls: [InGameControls.WEAPON_1,] })
    );
  });

  it("Should add more bullet when controls contains WEAPON_1", () => {
    let nextState = firePrimaryWeapon(recoverTimer)(
      makeGameState({
        weaponSlots: {
          primary: makeGun({ isReady: true }),
          secondary: makeGun({ isReady: false }),
        },
        bullets: [makeCharacter({ active: true, animationKey: AnimationKeys.BulletIdle })],
        controls: [InGameControls.WEAPON_1]
      })).extract();

    expect(nextState).toEqual(
      makeGameState({
        weaponSlots: {
          primary: makeGun({ isReady: false }),
          secondary: makeGun({ isReady: false }),
        },
        bullets: [
          makeCharacter({ active: true, animationKey: AnimationKeys.BulletIdle }),
          makeCharacter({ active: true, animationKey: AnimationKeys.BulletIdle }),
        ],
        controls: [InGameControls.WEAPON_1]
      })
    );
  });
});

describe("destroyBullet", () => {
  // remove bullet from list of bullet when dismissed from screen
  // GameState -> GameState
  // GameState(bullet:[], Screen(w: 10, h: 10)) -> GameState(bullet:[])
  // GameState(bullet:[Bullet(x:0,y:0)], Screen(w: 10, h: 10)) -> GameState(bullet:[Bullet(x:0,y:0)], Screen(w: 10, h: 10))
  // GameState(bullet:[Bullet(x:15,y:0)], Screen(w: 10, h: 10)) -> GameState(bullet:[], Screen(w: 10, h: 10))

  it("Should do nothing if bullets is empty", () => {
    let nextState = destroyBullet(
      makeGameState({
        bullets: [],
        camera: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
      })
    ).extract();
    expect(nextState).toEqual(
      makeGameState({
        bullets: [],
        camera: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
      })
    );
  });

  it("Should not remove bullet if bullet still in camera", () => {
    let nextState = destroyBullet(
      makeGameState({
        bullets: [makeCharacter({ x: 0, y: 0 })],
        camera: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
      })
    ).extract();
    expect(nextState).toEqual(
      makeGameState({
        bullets: [makeCharacter({ x: 0, y: 0 })],
        camera: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
      })
    );
  });

  it("Should remove bullet if bullet out of camera", () => {
    let nextState = destroyBullet(
      makeGameState({
        bullets: [makeCharacter({ x: 15, y: 0 })],
        camera: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
      })
    ).extract();
    expect(nextState).toEqual(
      makeGameState({
        bullets: [],
        camera: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
      })
    );
  });
});
