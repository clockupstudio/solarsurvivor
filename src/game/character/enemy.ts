import { Just, Maybe, Nothing } from "purify-ts";
import { Character, CharacterStates, Vector2 } from "./character";
import { findEventsByName, GameEvent, GameEventNames, getEventData, makeEnemyDeadEvent } from "../events/event";
import { GameState } from "../interfaces";
import { getListOfEnemyHitData, makeEnemyRecoveredEvent } from "../events/enemy_hit";
import { IPlayerRecoverStarter, IPlayStateContext } from "../states/play.state";
import { spawnEnemies } from "../level/level";

interface EnemiesEvents {
    enemies: Character[];
    inputEvents: GameEvent[];
}

export const updateEnemies = (context: IPlayStateContext) => (gameState: GameState): Maybe<GameState> => {
    return Maybe.of(gameState)
        .chain(spawnEnemies(context))
        .chain(updateEnemiesLife)
        .chain(handleEnemyHits(context))
        .chain(handleDeadEnemies(context))
        .chain(updateEnemyMovement);
}

const handleEnemyHits = (context: IPlayStateContext) => (gameState: GameState): Maybe<GameState> =>
    Maybe.of(gameState)
        .chain(markHitEnemies)
        .chain(startRecoveryTimers(context))
        .chain(markRecoveredEnemies);

const handleDeadEnemies = (context: IPlayStateContext) => (gameState: GameState): Maybe<GameState> =>
    Maybe.of(gameState)
        .chain(removeDeadEnemies)
        .chain(markDeadEnemies)
        .chain(emitEnemyDeathEvents(context))
        .chain(updateKillsCount(countDeadEnemies));


const updateEnemyMovement = (gameState: GameState): Maybe<GameState> =>
    Maybe.of(gameState)
        .chain(moveEnemies)
        .chain(applyEnemyKnockback);

// Generic ID matcher function
const matchById = <T extends { id: string }>(id: string) => (item: T): boolean =>
    item.id === id;

// Generic event data extractor
const getEventId = (event: GameEvent): string =>
    getEventData<{ id: string }>(event).id;

// Consolidated hit event finder
const findHitEvent = (id: string, events: GameEvent[]): Maybe<GameEvent> =>
    Maybe.fromNullable(
        findEventsByName(events, GameEventNames.ENEMY_HIT)
            .find(event => getEventId(event) === id)
    );

// Use these consolidated functions
export const filterByIdInAnotherList = (removingEnemies: Character[]) => (state: GameState): Character[] =>
    removingEnemies.length === 0
        ? state.enemies
        : state.enemies.filter(enemy =>
            removingEnemies.every(e => e.id !== enemy.id)
        );

export function applyEnemyKnockback(gameState: GameState): Maybe<GameState> {
    return Just({
        ...gameState,
        enemies: gameState.enemies.map(enemy =>
            findHitEvent(enemy.id, gameState.inputEvents)
                .map(() => applyKnockbackToEnemy(gameState.player)(enemy))
                .orDefault(enemy)
        )
    });
}

export const countHitEnemies = (gameState: EnemiesEvents): number =>
    gameState.enemies.filter(enemy =>
        findHitEvent(enemy.id, gameState.inputEvents).isJust()
    ).length;

// ====================
// Enemy Movement
// ====================

const canEnemyMove = (enemy: Character): boolean =>
    enemy.state === CharacterStates.IDLE;

const calculateNewPosition = (enemy: Character, player: Character) => ({
    ...enemy,
    x: calculateMovementDelta(enemy.x, enemy.speed)(player.x),
    y: calculateMovementDelta(enemy.y, enemy.speed)(player.y)
});

export function moveEnemies(gameState: GameState): Maybe<GameState> {
    return Just({
        ...gameState,
        enemies: gameState.enemies.map(enemy =>
            canEnemyMove(enemy)
                ? calculateNewPosition(enemy, gameState.player)
                : enemy
        )
    });
}

export function calculateMovementDelta(enemy: number, speed: number) {
    return (player: number) => {
        if (enemy === player) return enemy;
        return enemy < player ? enemy + speed : enemy - speed;
    }
}

// ====================
// Enemy State Management
// ====================
export const emitEnemyDeathEvents = (context: IPlayerRecoverStarter) => (gameState: GameState): Maybe<GameState> => {
    gameState.enemies
        .filter(enemy => enemy.state === CharacterStates.DEAD)
        .forEach(enemy => {
            context.startRecoverTimer(
                makeEnemyDeadEvent(enemy),
                0
            );
        });
    return Just(gameState);
};

export const startRecoveryTimers = (context: IPlayStateContext) => (gameState: GameState): Maybe<GameState> => {
    gameState.enemies
        .filter(enemy => enemy.state === CharacterStates.HIT)
        .forEach(enemy => {
            context.startRecoverTimer(
                makeEnemyRecoveredEvent(enemy.id),
                enemy.recoverTime
            );
        });
    return Just(gameState);
};

export const markRecoveredEnemies = (gameState: GameState): Maybe<GameState> => {
    return Maybe.of({
        ...gameState,
        enemies: gameState.enemies.map(enemy =>
            findEventsByName(gameState.inputEvents, GameEventNames.ENEMY_RECOVERED)
                .some(event => getEventData<{ id: string }>(event).id === enemy.id)
                ? { ...enemy, state: CharacterStates.IDLE }
                : enemy
        )
    });
};

export const markHitEnemies = (gameState: GameState): Maybe<GameState> => {
    return Maybe.of({
        ...gameState,
        enemies: gameState.enemies.map(enemy =>
            findHitEvent(enemy.id, gameState.inputEvents).map(() => ({ ...enemy, state: CharacterStates.HIT }))
                .orDefault(enemy)
        )
    });
};

export const markDeadEnemies = (gameState: GameState): Maybe<GameState> => {
    return Maybe.of({
        ...gameState,
        enemies: gameState.enemies.map((enemy) => {
            if (enemy.life <= 0) {
                return { ...enemy, state: CharacterStates.DEAD }; // Mark enemy as dead
            }
            return enemy;
        }),
    });
};

export const removeDeadEnemies = (gameState: GameState): Maybe<GameState> => {
    return Just({
        ...gameState,
        enemies: filterByIdInAnotherList(getDeadEnemies(gameState))(gameState)
    });
};

export const countDeadEnemies = (state: GameState): number => {
    return getDeadEnemies(state).length; // Count dead enemies
};

export const getDeadEnemies = (state: GameState): Character[] => {
    return state.enemies.filter((enemy) => enemy.state === CharacterStates.DEAD); // Get all dead enemies
};

export const updateEnemiesLife = (gameState: GameState): Maybe<GameState> => {
    const listOfEnemyHitData = getListOfEnemyHitData(gameState.inputEvents);

    return Maybe.of({
        ...gameState,
        enemies: gameState.enemies.map((enemy) => {
            const enemyHit = listOfEnemyHitData.find((enemyHitData) => enemyHitData.id === enemy.id);
            if (enemyHit) {
                return { ...enemy, life: enemy.life - enemyHit.damage }; // Update enemy life based on hit damage
            }
            return enemy; // Return enemy unchanged if not hit
        })
    });
};

// ====================
// Kills Count Management
// ====================

export const updateKillsCount = (counter: (state: GameState) => number) => (gameState: GameState): Maybe<GameState> => {
    return Maybe.of({
        ...gameState,
        killsCount: gameState.killsCount + counter(gameState) // Update the kills count based on the provided counter function
    });
};

export function knockback(character: Character, direction: Vector2, force: number): Character {
    return {
        ...character,
        x: character.x + (direction.x * force),
        y: character.y + (direction.y * force)
    };
}

// Helper functions for vector operations
const calculateDirection = (from: Vector2, to: Vector2): Maybe<Vector2> => {
    const dx = to.x - from.x;
    const dy = to.y - from.y;
    const length = Math.sqrt(dx * dx + dy * dy);

    return length === 0
        ? Nothing
        : Just({
            x: dx / length,
            y: dy / length
        });
};

const applyKnockbackToEnemy = (player: Character) => (enemy: Character): Character =>
    calculateDirection(player, enemy)
        .map(direction => knockback(enemy, direction, 8))
        .orDefault(enemy);
