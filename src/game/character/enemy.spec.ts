import { Character, CharacterStates, makeCharacter } from "../character/character";
import {
    calculateMovementDelta,
    countDeadEnemies,
    countHitEnemies,
    emitEnemyDeathEvents,
    getDeadEnemies,
    markDeadEnemies,
    markHitEnemies,
    moveEnemies,
    removeDeadEnemies,
    updateEnemiesLife
} from "../character/enemy";
import { GameEventNames, makeEnemyDeadEvent, makeGameEvent } from "../events/event";
import { makeGameState } from "../states/gamestate";
import { updateKillsCount } from "./enemy";
import { Just, Maybe } from "purify-ts";
import { makeEnemyHitEvent } from "../events/enemy_hit";
import { knockback, applyEnemyKnockback } from "./enemy";
import { Vector2 } from "../character/character";
import { IPlayerRecoverStarter } from "../states/play.state";


describe("moveEnemies", () => {
    [
        {
            name: "Empty enemies do nothing",
            given: makeGameState({ player: makeCharacter(), enemies: [] }),
            expect: makeGameState({ player: makeCharacter(), enemies: [] }),
        },
        {
            name: "Move enemies toward player's position from the left",
            given: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: -10, y: 0 })],
            }),
            expect: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: -9, y: 0 })],
            }),
        },
        {
            name: "Move enemies toward player's position from the right",
            given: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 10, y: 0 })],
            }),
            expect: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 9, y: 0 })],
            }),
        },
        {
            name: "Stop enemies when reached player's position",
            given: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 0, y: 0 })],
            }),
            expect: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 0, y: 0 })],
            }),
        },
        {
            name: "Move enemies toward player's position from the top",
            given: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 0, y: -10 })],
            }),
            expect: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 0, y: -9 })],
            }),
        },
        {
            name: "Move enemies toward player's position from the bottom",
            given: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 0, y: 10 })],
            }),
            expect: makeGameState({
                player: makeCharacter({ x: 0, y: 0 }),
                enemies: [makeCharacter({ x: 0, y: 9 })],
            }),
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            expect(moveEnemies(testCase.given).extract()).toEqual(testCase.expect);
        });
    });
});

describe("calculateMovementDelta", () => {
    const targetPosition = -10;
    const targetSpeed = 1;
    const destinationPosition = 0;
    const expectedDelta = -9;
    it("increase value when target is on the left of destination", () => {
        expect(
            calculateMovementDelta(targetPosition, targetSpeed)(destinationPosition)
        ).toEqual(expectedDelta);
    });
});

describe("updateKillsCount", () => {
    [
        {
            name: "Empty enemy hit, Empty enemy list -> should not increase kills count",
            enemyHit: [],
            enemies: [],
            expectedEnemies: [],
            expectedKillsCount: 0
        },
        {
            name: "Empty enemy hit, 1 enemy list -> return same enemy list",
            enemyHit: [],
            enemies: [makeCharacter()],
            expectedEnemies: [makeCharacter()],
            expectedKillsCount: 0
        },
        {
            name: "1 enemy hit, 1 enemy and not match -> same enemy list",
            enemyHit: [makeGameEvent(GameEventNames.ENEMY_HIT, { id: "1234" })],
            enemies: [makeCharacter({ id: "5678" })],
            expectedEnemies: [makeCharacter({ id: "5678" })],
            expectedKillsCount: 0
        },
        {
            name: "1 enemy hit, 1 enemy and matched -> empty enemy list",
            enemyHit: [makeGameEvent(GameEventNames.ENEMY_HIT, { id: "1234" })],
            enemies: [makeCharacter({ id: "1234" })],
            expectedEnemies: [makeCharacter({ id: "1234" })],
            expectedKillsCount: 1
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            const givenState = makeGameState({ inputEvents: testCase.enemyHit, enemies: testCase.enemies });
            const expectedState = makeGameState({
                inputEvents: testCase.enemyHit,
                enemies: testCase.expectedEnemies,
                killsCount: testCase.expectedKillsCount
            });

            expect(updateKillsCount(countHitEnemies)(givenState)).toEqual(Maybe.of(expectedState));
        });
    });

});

describe('countDeadEnemies', () => {
    [
        {
            name: 'given empty enemy list should return 0',
            enemies: [],
            expected: 0,
        },
        {
            name: 'given 1 live enemy in the list should return 0',
            enemies: [makeCharacter({ state: CharacterStates.IDLE })],
            expected: 0,
        },
        {
            name: 'given 1 dead enemy in the list should return 0',
            enemies: [makeCharacter({ state: CharacterStates.DEAD })],
            expected: 1,
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            expect(
                countDeadEnemies(
                    makeGameState({ enemies: testCase.enemies })))
                .toEqual(testCase.expected);
        });
    });
})

describe('getDeadEnemies', () => {
    [
        {
            name: 'return empty list when given empty list',
            enemies: [],
            expected: [],
        },
        {
            name: 'return empty list when given list with no dead enemy',
            enemies: [makeCharacter({ state: CharacterStates.IDLE })],
            expected: [],
        },
        {
            name: 'return list of dead enemy when given list with dead enemy',
            enemies: [makeCharacter({ state: CharacterStates.DEAD })],
            expected: [makeCharacter({ state: CharacterStates.DEAD })],
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            expect(getDeadEnemies(makeGameState({ enemies: testCase.enemies }))).toEqual(testCase.expected);
        });
    })
});

describe('removeDeadEnemies', () => {
    [
        {
            name: 'empty list returns empty list',
            enemies: [],
            expected: [],
        },
        {
            name: 'empty list with 1 live enemy returns the same list',
            enemies: [makeCharacter({ state: CharacterStates.IDLE })],
            expected: [makeCharacter({ state: CharacterStates.IDLE })],
        },
        {
            name: 'empty list with 1 dead enemy returns empty list',
            enemies: [makeCharacter({ state: CharacterStates.DEAD })],
            expected: [],
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            expect(removeDeadEnemies(makeGameState({ enemies: testCase.enemies })))
                .toEqual(Maybe.of(makeGameState({ enemies: testCase.expected })))
        });
    });
});

describe('updateEnemiesLife', () => {
    [
        {
            name: 'Empty events do nothing',
            enemies: [],
            inputEvents: [],
            expect: []
        },
        {
            name: '1 of enemy hit matched with 1 enemy should reduce enemy HP by damage value',
            enemies: [makeCharacter({ id: '1234', life: 1 })],
            inputEvents: [makeEnemyHitEvent({ damage: 1, id: '1234' })],
            expect: [makeCharacter({ id: '1234', life: 0 })]
        },
        {
            name: 'No enemy hit matched with 1 enemy should reduce enemy HP by damage value',
            enemies: [makeCharacter({ id: '1234', life: 1 })],
            inputEvents: [makeEnemyHitEvent({ damage: 1, id: 'AAAA' })],
            expect: [makeCharacter({ id: '1234', life: 1 })]
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            expect(updateEnemiesLife(makeGameState({ ...testCase })))
                .toEqual(Maybe.of(makeGameState({ ...testCase, enemies: testCase.expect })))
        });
    })
});

describe('markHitEnemies', () => {
    it('should set enemies state to HIT if find enemy hit event', () => {
        const enemy = makeCharacter({ id: '1234', state: CharacterStates.IDLE });
        const gameState = makeGameState({ enemies: [enemy], inputEvents: [makeEnemyHitEvent({ id: '1234', damage: 1 })] });
        expect(markHitEnemies(gameState))
            .toEqual(
                Maybe.of(makeGameState({
                    enemies: [makeCharacter({ ...enemy, state: CharacterStates.HIT })],
                    inputEvents: [makeEnemyHitEvent({ id: '1234', damage: 1 })]
                })));
    });
});

describe('markDeadEnemies', () => {
    [
        {
            name: 'do nothing if enemies is empty',
            enemies: [],
            expect: [],
        },
        {
            name: 'do nothing if enemies life is greater than 0',
            enemies: [makeCharacter({ life: 1, state: CharacterStates.IDLE })],
            expect: [makeCharacter({ life: 1, state: CharacterStates.IDLE })],
        },
        {
            name: 'set enemies state to DEAD if enemies life is equal to 0',
            enemies: [makeCharacter({ life: 0, state: CharacterStates.IDLE })],
            expect: [makeCharacter({ life: 0, state: CharacterStates.DEAD })],
        },
        {
            name: 'set enemies state to DEAD if enemies life is lower than 0',
            enemies: [makeCharacter({ life: -999, state: CharacterStates.IDLE })],
            expect: [makeCharacter({ life: -999, state: CharacterStates.DEAD })],
        },
    ].forEach((testCase) => {
        it(testCase.name, () => {
            expect(markDeadEnemies(makeGameState({ enemies: testCase.enemies })))
                .toEqual(Maybe.of(makeGameState({ enemies: testCase.expect })))
        });
    });

});

describe('knockback', () => {
    it('should move character by direction and force', () => {
        const character = makeCharacter({ x: 0, y: 0 });
        const direction: Vector2 = { x: 1, y: 0 };
        const force = 5;

        expect(knockback(character, direction, force)).toEqual(
            makeCharacter({ x: 5, y: 0 })
        );
    });

    it('should move character diagonally', () => {
        const character = makeCharacter({ x: 0, y: 0 });
        const direction: Vector2 = { x: 1, y: 1 };
        const force = 5;

        expect(knockback(character, direction, force)).toEqual(
            makeCharacter({ x: 5, y: 5 })
        );
    });
});

describe('applyEnemyKnockback', () => {
    it('should not modify enemies without hit events', () => {
        const enemy = makeCharacter({ id: '1', x: 0, y: 0 });
        const gameState = makeGameState({
            enemies: [enemy],
            player: makeCharacter({ x: 0, y: 0 }),
            inputEvents: []
        });

        expect(applyEnemyKnockback(gameState).extract()).toEqual(gameState);
    });

    it('should knock enemy away from player when hit', () => {
        const enemy = makeCharacter({ id: '1', x: 0, y: 0 });
        const player = makeCharacter({ x: -10, y: 0 }); // Player is to the left
        const gameState = makeGameState({
            enemies: [enemy],
            player: player,
            inputEvents: [makeGameEvent(GameEventNames.ENEMY_HIT, { id: '1' })]
        });

        const result = applyEnemyKnockback(gameState).extract();
        const knockedEnemy = result?.enemies[0];

        // Enemy should be knocked to the right (away from player)
        expect(knockedEnemy?.x).toBeGreaterThan(enemy.x);
        // Y position should remain same since player is directly to the left
        expect(knockedEnemy?.y).toBe(enemy.y);
    });

    it('should only knock back hit enemies', () => {
        const hitEnemy = makeCharacter({ id: '1', x: 0, y: 0 });
        const unhitEnemy = makeCharacter({ id: '2', x: 0, y: 0 });
        const player = makeCharacter({ x: -10, y: 0 });

        const gameState = makeGameState({
            enemies: [hitEnemy, unhitEnemy],
            player: player,
            inputEvents: [makeGameEvent(GameEventNames.ENEMY_HIT, { id: '1' })]
        });

        const result = applyEnemyKnockback(gameState).extract();

        // Hit enemy should move
        expect(result?.enemies[0].x).toBeGreaterThan(hitEnemy.x);
        // Unhit enemy should stay in place
        expect(result?.enemies[1]).toEqual(unhitEnemy);
    });
});

describe('emitEnemyDeathEvents', () => {

    let mockContext: IPlayerRecoverStarter;

    const createEnemy = (id: string, state: CharacterStates) => (makeCharacter({
        id,
        state,
    }));

    const createGameState = (enemies: Character[]) => (makeGameState({
        enemies,
    }));

    beforeEach(() => {
        mockContext = {
            startRecoverTimer: jest.fn(),
        };
    });

    const testCases = [
        {
            name: 'single dead enemy',
            enemies: [
                { id: 'enemy-1', state: CharacterStates.DEAD }
            ],
            expectedCalls: 1,
            expectedEvents: [{ enemyId: 'enemy-1' }]
        },
        {
            name: 'multiple enemies with one dead',
            enemies: [
                { id: 'enemy-1', state: CharacterStates.DEAD },
                { id: 'enemy-2', state: CharacterStates.IDLE },
                { id: 'enemy-3', state: CharacterStates.HIT }
            ],
            expectedCalls: 1,
            expectedEvents: [{ enemyId: 'enemy-1' }]
        },
        {
            name: 'multiple dead enemies',
            enemies: [
                { id: 'enemy-1', state: CharacterStates.DEAD },
                { id: 'enemy-2', state: CharacterStates.DEAD },
            ],
            expectedCalls: 2,
            expectedEvents: [
                { enemyId: 'enemy-1' },
                { enemyId: 'enemy-2' }
            ]
        },
        {
            name: 'no dead enemies',
            enemies: [
                { id: 'enemy-1', state: CharacterStates.IDLE },
                { id: 'enemy-2', state: CharacterStates.HIT }
            ],
            expectedCalls: 0,
            expectedEvents: []
        },
        {
            name: 'empty enemies array',
            enemies: [],
            expectedCalls: 0,
            expectedEvents: []
        }
    ];

    test.each(testCases)('$name', ({ enemies, expectedCalls, expectedEvents }) => {
        // Arrange
        const gameState = createGameState(
            enemies.map(e => createEnemy(e.id, e.state))
        );

        // Act
        const result = emitEnemyDeathEvents(mockContext)(gameState);

        // Assert
        expect(result).toEqual(Just(gameState));
        expect(mockContext.startRecoverTimer).toHaveBeenCalledTimes(expectedCalls);

        // Verify each expected event call
        expectedEvents.forEach(({ enemyId }) => {
            const expectedEnemy = enemies.find(e => e.id === enemyId);
            expect(mockContext.startRecoverTimer).toHaveBeenCalledWith(
                makeEnemyDeadEvent(createEnemy(enemyId, expectedEnemy!.state)),
                0
            );
        });
    });
});