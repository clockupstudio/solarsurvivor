export enum AnimationKeys {
  PlayerMovingLeft = "playerMovingLeft",
  PlayerMovingRight = "playerMovingRight",
  PlayerHit = "playerHit",
  PlayerDead = "playerDead",
  BulletIdle = "bulletIdle",
  EnemyIdle = "enemyIdle",
  Enemy2Idle = "enemy2Idle",
  ItemCoin = "ItemCoin",
  ItemHealth = "ItemHealth",
}
export const animations = [
  {
    key:AnimationKeys.PlayerMovingLeft,
    frames: { texture: "player", frames: { start: 0, end: 0 }},
    frameRate: 60,
    repeat: -1,
  },
  {
    key: AnimationKeys.PlayerMovingRight,
    frames: { texture: "player", frames: { start: 1, end: 1 }},
    frameRate: 60,
    repeat: -1,
  },
  {
    key: AnimationKeys.PlayerHit,
    frames: { texture: "player", frames: { start: 4, end: 4 }},
    frameRate: 60,
    repeat: -1,
  },
  {
    key: AnimationKeys.PlayerDead,
    frames: { texture: "player", frames: { start: 5, end: 8 }},
    frameRate: 4,
    repeat: 0,
  },
  {
    key: AnimationKeys.BulletIdle,
    frames: { texture: "normalBullet", frames: { start: 0, end: 0 }},
    frameRate: 60,
    repeat: -1,
  },
  {
    key: AnimationKeys.EnemyIdle,
    frames: { texture: "enemy", frames: { start: 0, end: 1 }},
    frameRate: 2,
    repeat: -1,
  },
  {
    key: AnimationKeys.Enemy2Idle,
    frames: { texture: "enemy", frames: { start: 2, end: 3 }},
    frameRate: 2,
    repeat: -1,
  },
  {
    key: AnimationKeys.ItemHealth,
    frames: { texture: "item", frames: { start: 0, end: 0 }},
    frameRate: 60,
    repeat: -1,
  },
  {
    key: AnimationKeys.ItemCoin,
    frames: { texture: "item", frames: { start: 1, end: 1 }},
    frameRate: 60,
    repeat: -1,
  },
]
