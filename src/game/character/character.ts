import { GameCamera } from "../camera/camera";
import { InGameControls } from "../controls";
import { Bounds } from "../interfaces";

export interface Character {
  recoverTime: number;
  x: integer;
  y: integer;
  speed: number;
  direction: Vector2;
  shootingDirection: Vector2;
  animationKey: string;
  active: boolean;
  offset: number;
  id: string;
  state: CharacterStates;
  life: number;
}

export enum CharacterStates {
  IDLE, HIT, DEAD
}

export interface Vector2 {
  x: number;
  y: number;
}

export const Directions  = {
  LEFT: { x: -1, y: 0},
  RIGHT: { x: 1, y: 0},
  UP: { x: 0, y: -1},
  DOWN: { x: 0, y: 1},
}

export interface MakeCharacterParams {
  x?: integer;
  y?: integer;
  speed?: number;
  direction?: Vector2;
  shootingDirection?: Vector2;
  animationKey?: string;
  active?: boolean;
  offset?: number;
  id?: string;
  state?: CharacterStates;
  life?: number;
  recoverTime?: number;
}

export function makeCharacter({
  x = 0,
  y = 0,
  speed = 1,
  direction = Directions.LEFT,
  shootingDirection = Directions.LEFT,
  animationKey = "",
  active = false,
  offset = 0,
  id = "",
  state = CharacterStates.IDLE,
  life = 1,
  recoverTime = 1000,
}: MakeCharacterParams = {}) {
  return {
    x: x,
    y: y,
    speed: speed,
    direction: direction,
    shootingDirection: shootingDirection,
    animationKey: animationKey,
    active: active,
    offset: offset,
    id: id,
    state: state,
    life: life,
    recoverTime: recoverTime,
  };
}

// Calculate new position of the character by the given controls
export function moveCharacter(
  character: Character,
  controls: InGameControls[]
): Character {
  let x = controls.includes(InGameControls.LEFT)
    ? character.x - character.speed
    : character.x;
  x = controls.includes(InGameControls.RIGHT) ? character.x + character.speed : x;

  let y = controls.includes(InGameControls.UP)
    ? character.y - character.speed
    : character.y;
  y = controls.includes(InGameControls.DOWN) ? character.y + character.speed : y;

  return { ...character, x: x, y: y };
}


export function clampToBounds(
  character: Character,
  bounds: Bounds
): Character {
  return {
    ...character,
    x: Math.max(bounds.left, Math.min(bounds.right - character.offset, character.x)),
    y: Math.max(bounds.top, Math.min(bounds.bottom - character.offset, character.y))
  };
}

export function updateDirection(
  character: Character,
  controls: InGameControls[]
): Character {
  let direction = controls.includes(InGameControls.RIGHT)
    ? Directions.RIGHT
    : character.direction;
  direction = controls.includes(InGameControls.LEFT) ? Directions.LEFT : direction;
  direction = controls.includes(InGameControls.UP) ? Directions.UP : direction;
  direction = controls.includes(InGameControls.DOWN) ? Directions.DOWN : direction;

  return {
    ...character,
    direction: direction,
  };
}