import { Just, Maybe } from "purify-ts";
import {
  Character,
  CharacterStates,
  clampToBounds,
  Directions,
  moveCharacter,
  updateDirection,
} from "../character/character";
import {
  GameEventNames,
  PlayerHitEventData,
  findEventsByName,
  findFirstEventByName,
  getEventData,
} from "../events/event";
import { AnimationKeys } from "./animations";
import { GameState } from "../interfaces";
import { InGameControls } from "../controls";

export const readPlayerHit = (gameState: GameState): Maybe<GameState> => {
  const processHit = (state: GameState) =>
    Just(state)
      .chain(reducePlayerLife)
      .chain(switchToHitState)
      .chain(switchToDeadState);

  return isPlayerIdle(gameState)
    ? findFirstEventByName(gameState.inputEvents, GameEventNames.PLAYER_HIT)
        .map(() => processHit(gameState))
        .orDefault(Just(gameState))
    : Just(gameState);
};

const isPlayerIdle = (state: GameState): boolean => 
  state.player.state === CharacterStates.IDLE;

export const readPlayerRecovered = (gameState: GameState) =>
  findEventsByName(gameState.inputEvents, GameEventNames.PLAYER_RECOVERED)
    .length > 0
    ? Just({
        ...gameState,
        player: { ...gameState.player, state: CharacterStates.IDLE },
      })
    : Just(gameState);

export const updatePlayer = (gameState: GameState) =>
  gameState.player.state === CharacterStates.IDLE
    ? Just({
        ...gameState,
        player: updateShootingDirection(
          updateAnimation(
            updateDirection(
              clampToBounds(
                moveCharacter(gameState.player, gameState.controls),
                gameState.camera.bounds
              ),
              gameState.controls
            )
          ),
          gameState.controls
        ),
      })
    : Just(gameState)
      .chain(playHitAnimation).chain(playDeadAnimation);

function switchToHitState(gameState: GameState) {
  return Just({
    ...gameState,
    player: { ...gameState.player, state: CharacterStates.HIT },
  });
}

function switchToDeadState(gameState: GameState) {
  return gameState.player.life === 0
    ? Just({
        ...gameState,
        player: { ...gameState.player, state: CharacterStates.DEAD },
      })
    : Just(gameState);
}

function playHitAnimation(gameState: GameState) {
  return Just({
    ...gameState,
    player: {
      ...gameState.player,
      animationKey: AnimationKeys.PlayerHit,
    },
  });
}

export function updateAnimation(character: Character): Character {
  let animationKey =
    character.direction === Directions.LEFT
      ? "playerMovingLeft"
      : character.animationKey;
  animationKey =
    character.direction === Directions.RIGHT
      ? "playerMovingRight"
      : animationKey;
  return {
    ...character,
    animationKey: animationKey,
  };
}

function reducePlayerLife(gameState: GameState) {
  return Just({
    ...gameState,
    player: {
      ...gameState.player,
      life: gameState.player.life - getDamage(),
    },
  });

  function getDamage() {
    return getEventData<PlayerHitEventData>(
      findEventsByName(gameState.inputEvents, GameEventNames.PLAYER_HIT)[0]
    ).damage;
  }
}

function playDeadAnimation(gameState: GameState): Maybe<GameState> {
  return gameState.player.state === CharacterStates.DEAD? 
    Just({ 
      ...gameState,
      player: {
        ...gameState.player,
        animationKey: AnimationKeys.PlayerDead
      }
    }):
    Just(gameState)
}

export function increasePlayerLife(gameState: GameState): GameState {
  return {
      ...gameState,
      player: {
          ...gameState.player,
          life: gameState.player.life + 1
      }
  };
}

export function updateShootingDirection(
  character: Character,
  controls: InGameControls[]
): Character {
  if (controls.includes(InGameControls.WEAPON_1)) {
    return character;
  }
  return {
    ...character,
    shootingDirection: character.direction
  };
}