
import { makeGameCamera } from "../camera/camera";
import { makeCharacter, Directions, CharacterStates } from "./character";
import { InGameControls } from "../controls";
import { makeGameEvent, GameEventNames, makePlayerHitEventData } from "../events/event";
import { readPlayerHit, readPlayerRecovered, updateAnimation, updatePlayer, updateShootingDirection } from "./player";
import { makeGameState } from "../states/gamestate";
import { AnimationKeys } from "./animations";


describe("updateAnimation", () => {
  it("animationKey should be playerMovingLeft when direction is left", () => {
    expect(
      updateAnimation(makeCharacter({ direction: Directions.LEFT }))
    ).toEqual(
      makeCharacter({
        direction: Directions.LEFT,
        animationKey: AnimationKeys.PlayerMovingLeft,
      })
    );
  });

  it("animation key should be playerMovingRight when direction is right", () => {
    expect(
      updateAnimation(makeCharacter({ direction: Directions.RIGHT }))
    ).toEqual(
      makeCharacter({
        direction: Directions.RIGHT,
        animationKey: AnimationKeys.PlayerMovingRight,
      })
    );
  });

  it("animation key should not change when direction is UP", () => {
    expect(
      updateAnimation(
        makeCharacter({
          direction: Directions.UP,
          animationKey: "playerMovingLeft",
        })
      )
    ).toEqual(
      makeCharacter({
        direction: Directions.UP,
        animationKey: "playerMovingLeft",
      })
    );
  });
});

describe("readPlayerHit", () => {
  // No player hit
  // Player hit, reduce player life
  // TODO: Player hit, life > 0 -> set player state to HIT
  // TODO: Player hit, life = 0 -> set player state to DEAD
  [
    {
      name: "No event return same state",
      input: makeGameState(),
      expected: makeGameState(),
    },
    {
      name: "Player not in idle state, should return same state",
      input: makeGameState({
        inputEvents: [makeGameEvent(GameEventNames.PLAYER_HIT, makePlayerHitEventData(1))],
        player: makeCharacter({ state: CharacterStates.HIT }),
      }),
      expected: makeGameState({
        inputEvents: [makeGameEvent(GameEventNames.PLAYER_HIT, makePlayerHitEventData(1))],
        player: makeCharacter({ state: CharacterStates.HIT }),
      }),
    },
    {
      name: "Player hit and life > 0 should set player state to HIT",
      input: makeGameState({
        inputEvents: [
          makeGameEvent(GameEventNames.PLAYER_HIT, makePlayerHitEventData(1)),
        ],
        player: makeCharacter({ life: 2 })
      }),
      expected: makeGameState({
        inputEvents: [
          makeGameEvent(GameEventNames.PLAYER_HIT, makePlayerHitEventData(1)),
        ],
        player: makeCharacter({ state: CharacterStates.HIT, life: 1 }),
      }),
    },
    {
      name: "Player hit and life = 0 should set player state to DEAD",
      input: makeGameState({
        inputEvents: [
          makeGameEvent(GameEventNames.PLAYER_HIT, makePlayerHitEventData(1)),
        ],
        player: makeCharacter({ life: 1 })
      }),
      expected: makeGameState({
        inputEvents: [
          makeGameEvent(GameEventNames.PLAYER_HIT, makePlayerHitEventData(1)),
        ],
        player: makeCharacter({ state: CharacterStates.DEAD, life: 0 }),
      }),
    },
  ].forEach((tc) => {
    it(tc.name, () => {
      expect(readPlayerHit(tc.input).extract()).toEqual(tc.expected);
    });
  });
});

describe("readPlayerRecovered", () => {
  [
    {
      name: "No event return same state",
      input: makeGameState(),
      expected: makeGameState(),
    },
    {
      name: "Player recovered, set player state to IDLE",
      input: makeGameState({
        inputEvents: [makeGameEvent(GameEventNames.PLAYER_RECOVERED, {})],
        player: makeCharacter({ state: CharacterStates.HIT }),
      }),
      expected: makeGameState({
        inputEvents: [makeGameEvent(GameEventNames.PLAYER_RECOVERED, {})],
        player: makeCharacter({ state: CharacterStates.IDLE }),
      }),
    },
  ].forEach((testCase) => {
    it(testCase.name, () => {
      expect(readPlayerRecovered(testCase.input).extract()).toEqual(
        testCase.expected
      );
    });
  });
});

describe("updatePlayer", () => {
  it("should keep player within camera bounds", () => {
    const gameState = makeGameState({
      player: makeCharacter({ 
        x: -5, 
        y: -5, 
        offset: 10,
        state: CharacterStates.IDLE 
      }),
      camera: makeGameCamera({
        bounds: {
          left: 0,
          right: 800,
          top: 0,
          bottom: 600
        }
      }),
      controls: [InGameControls.LEFT, InGameControls.UP]
    });

    const result = updatePlayer(gameState).extract();

    expect(result?.player?.x).toBe(0);
    expect(result?.player?.y).toBe(0);
  });

  [
    {
      name: "Set playerAnimationLeft by default",
      input: makeGameState(),
      expect: makeGameState({
        player: makeCharacter({ animationKey: AnimationKeys.PlayerMovingLeft }),
      }),
    },
    {
      name: "Set animation to playerHit when character state is HIT",
      input: makeGameState({
        player: makeCharacter({ state: CharacterStates.HIT }),
      }),
      expect: makeGameState({
        player: makeCharacter({
          state: CharacterStates.HIT,
          animationKey: AnimationKeys.PlayerHit,
        }),
      }),
    },
    {
      name: "Set animation to playerDead when character state is DEAD",
      input: makeGameState({
        player: makeCharacter({ state: CharacterStates.DEAD }),
      }),
      expect: makeGameState({
        player: makeCharacter({
          state: CharacterStates.DEAD,
          animationKey: AnimationKeys.PlayerDead,
        }),
      }),
    },
    {
      name: "Set shooting direction to match movement when not shooting",
      input: makeGameState({
        controls: [InGameControls.LEFT],
        player: makeCharacter({ 
          direction: Directions.LEFT, 
          shootingDirection: Directions.RIGHT 
        }),
      }),
      expect: makeGameState({
        controls: [InGameControls.LEFT],
        player: makeCharacter({ 
          direction: Directions.LEFT, 
          shootingDirection: Directions.LEFT,
          animationKey: AnimationKeys.PlayerMovingLeft,
          x: 0,
        }),
      }),
    },
    {
      name: "Shooting direction should not change when SHOOT control is active",
      input: makeGameState({
        controls: [InGameControls.LEFT, InGameControls.WEAPON_1],
        player: makeCharacter({ 
          direction: Directions.LEFT, 
          shootingDirection: Directions.RIGHT 
        }),
      }),
      expect: makeGameState({
        controls: [InGameControls.LEFT, InGameControls.WEAPON_1],
        player: makeCharacter({ 
          direction: Directions.LEFT, 
          shootingDirection: Directions.RIGHT,
          animationKey: AnimationKeys.PlayerMovingLeft,
          x: 0,
        }),
      }),
    },
  ].forEach((testCase) => {
    it(testCase.name, () => {
      expect(updatePlayer(testCase.input).extract()).toEqual(testCase.expect);
    });
  });
});

describe("updateShootingDirection", () => {
  const testCases = [
    {
      name: "should not update shooting direction when WEAPON_1 control is active",
      character: makeCharacter({
        direction: Directions.RIGHT,
        shootingDirection: Directions.LEFT
      }),
      controls: [InGameControls.WEAPON_1, InGameControls.RIGHT],
      expected: Directions.LEFT  // shooting direction remains unchanged
    },
    {
      name: "should update shooting direction to match movement when not shooting",
      character: makeCharacter({
        direction: Directions.RIGHT,
        shootingDirection: Directions.LEFT
      }),
      controls: [InGameControls.RIGHT],
      expected: Directions.RIGHT  // shooting direction updates to match movement
    },
    {
      name: "should maintain same direction when no controls active",
      character: makeCharacter({
        direction: Directions.UP,
        shootingDirection: Directions.UP
      }),
      controls: [],
      expected: Directions.UP
    }
  ];

  testCases.forEach(({ name, character, controls, expected }) => {
    it(name, () => {
      const result = updateShootingDirection(character, controls);
      expect(result.shootingDirection).toEqual(expected);
    });
  });
});