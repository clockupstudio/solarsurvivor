import { Character } from "../character/character";
import { Bounds } from "../interfaces";
import { GameCamera } from "./camera";

function getBounds(target: Character): Bounds {
  return {
    left: target.x,
    right: target.x + target.offset,
    top: target.y,
    bottom: target.y + target.offset
  };
}

function getViewportBounds(camera: GameCamera): Bounds {
  return {
    left: camera.x,
    right: camera.x + camera.viewportWidth,
    top: camera.y,
    bottom: camera.y + camera.viewportHeight
  };
}

export function isInCamera(target: Character, camera: GameCamera): boolean {
  const targetBounds = getBounds(target);
  const viewportBounds = getViewportBounds(camera);

  return (
    targetBounds.left <= viewportBounds.right && 
    targetBounds.right >= viewportBounds.left &&
    targetBounds.top <= viewportBounds.bottom && 
    targetBounds.bottom >= viewportBounds.top
  );
}