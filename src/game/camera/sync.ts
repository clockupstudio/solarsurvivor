import { Maybe, Just } from "purify-ts";
import { GameState, IGameSceneCamera } from "../interfaces";

export function syncCamera(gameScene: IGameSceneCamera) {
    return (state: GameState): Maybe<GameState> => {
      const phaserCamera = gameScene.cameras.main;
      
      // Calculate new camera position within bounds
      const newX = clamp(
        phaserCamera.scrollX,
        state.camera.bounds.left,
        state.camera.bounds.right - state.camera.viewportWidth
      );
      
      const newY = clamp(
        phaserCamera.scrollY,
        state.camera.bounds.top,
        state.camera.bounds.bottom - state.camera.viewportHeight
      );
  
      return Just({
        ...state,
        camera: {
          ...state.camera,
          x: newX,
          y: newY
        }
      });
    };
  }
  
  function clamp(value: number, min: number, max: number): number {
    return Math.max(min, Math.min(value, max));
  }