import { makeGameCamera } from "./camera";

describe('GameCamera', () => {
    it('should create camera with default values', () => {
        const camera = makeGameCamera();
        expect(camera).toEqual({
            x: 0,
            y: 0,
            viewportWidth: 800,
            viewportHeight: 600,
            bounds: {
                left: 0,
                right: 2400,
                top: 0,
                bottom: 1800
            },
        });
    });

    it('should create camera with custom values', () => {
        const camera = makeGameCamera({
            viewportWidth: 1000,
            viewportHeight: 800,
            bounds: {
                left: 0,
                right: 2000,
                top: 0,
                bottom: 1500
            }
        });
        
        expect(camera.viewportWidth).toBe(1000);
        expect(camera.viewportHeight).toBe(800);
        expect(camera.bounds.right).toBe(2000);
        expect(camera.bounds.bottom).toBe(1500);
  });
});
