export interface GameCamera {
    x: number;
    y: number;
    viewportWidth: number;
    viewportHeight: number;
    bounds: Bounds;
}

interface Bounds {
    left: number;
    right: number;
    top: number;
    bottom: number;
}

export function makeGameCamera(params: Partial<GameCamera> = {}): GameCamera {
    return {
        x: params.x || 0,
        y: params.y || 0,
        viewportWidth: params.viewportWidth || 800,
        viewportHeight: params.viewportHeight || 600,
        bounds: params.bounds || {
            left: 0,
            right: 2400,
            top: 0,
            bottom: 1800
        },
    };
}
