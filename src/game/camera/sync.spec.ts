import { makeGameState, GameStates } from "../states/gamestate";
import { makeGameCamera } from "./camera";
import { syncCamera } from "./sync";


describe("syncCamera", () => {
  const mockGameScene = {
    cameras: {
      main: {
        scrollX: 0,
        scrollY: 0
      }
    }
  };

  it("should sync camera position with phaser camera", () => {
    // Arrange
    const camera = makeGameCamera({
      viewportWidth: 800,
      viewportHeight: 600,
      bounds: {
        left: 0,
        right: 2400,
        top: 0,
        bottom: 1800
      }
    });

    const initialState = makeGameState({
      state: GameStates.Play,
      camera
    });

    mockGameScene.cameras.main = {
      scrollX: 100,
      scrollY: 150
    };

    // Act
    const result = syncCamera(mockGameScene)(initialState).extract();

    // Assert
    expect(result?.camera.x).toBe(100);
    expect(result?.camera.y).toBe(150);
  });

  it("should respect camera bounds when syncing", () => {
    // Arrange
    const camera = makeGameCamera({
      viewportWidth: 800,
      viewportHeight: 600,
      bounds: {
        left: 0,
        right: 1000,
        top: 0,
        bottom: 1000
      }
    });

    const initialState = makeGameState({
      state: GameStates.Play,
      camera
    });

    mockGameScene.cameras.main = {
      scrollX: 1200,
      scrollY: -100
    };

    // Act
    const result = syncCamera(mockGameScene)(initialState).extract();

    // Assert
    expect(result?.camera.x).toBe(200); // 1000 - 800
    expect(result?.camera.y).toBe(0);
  });
});
