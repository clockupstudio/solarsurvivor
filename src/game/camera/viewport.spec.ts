import { makeCharacter } from "../character/character";
import { makeGameCamera } from "./camera";
import { isInCamera } from "./viewport";

describe("isInCamera", () => {
    // Detect is character position not in hte camera's frame
    // Bullet, Camera -> Bool
    // Character(x:0, y:0, offset: 5), Camera(width: 10, height: 10) -> true
    // Character(x:5, y:5, offset: 5), Camera(width: 10, height: 10) -> true
    // Character(x:-5, y:0, offset: 5), Camera(width: 10, height: 10) -> false
    [
      {
        name: "Should be true when character is at the top left corner of screen",
        character: { x: 0, y: 0 },
        screen: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
        expected: true,
      },
      {
        name: "Should be true when character is at the center of the screen",
        character: { x: 5, y: 5 },
        screen: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
        expected: true,
      },
      {
        name: "Should be false when character is lower than the left edge of the screen",
        character: { x: -5, y: 5 },
        screen: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
        expected: false,
      },
      {
        name: "Should be false when character is greater than the right edge of the screen",
        character: { x: 15, y: 5 },
        screen: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
        expected: false,
      },
      {
        name: "Should be false when character is over the top edge of the screen",
        character: { x: 5, y: -5 },
        screen: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
        expected: false,
      },
      {
        name: "Should be false when character is over the bottom edge of the screen",
        character: { x: 5, y: 15 },
        screen: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
        expected: false,
      },
      {
        name: "Should consider character's offset",
        character: { x: 5, y: 5, offset: 10 },
        screen: makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
        expected: true,
      }
    ].forEach((testCase) =>
      it(testCase.name, () => {
        expect(
          isInCamera(makeCharacter(testCase.character), testCase.screen)
        ).toEqual(testCase.expected);
      })
    );
  });