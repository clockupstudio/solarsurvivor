import { GameState } from "./interfaces";
import { makeCharacter } from "./character/character";
import { makeGun } from "./character/bullet";
import { guns } from "./data/guns";
import { GameStates } from "./states/gamestate";
import config from "../config";
import { AnimationKeys } from "./character/animations";
import { makeGameCamera } from "./camera/camera";

interface InitialGameStateParams {
  x: number;
  y: number;
}

export function makeInitialGameState({ x, y }: InitialGameStateParams): GameState {
  return {
    player: makeCharacter({
      x,
      y,
      speed: 1,
      active: true,
      offset: 12,
      life: 5,
      recoverTime: 500,
    }),
    bullets: [],
    enemies: [],
    guns: guns.map((data) => makeGun(data)),
    weaponSlots: {
      primary: makeGun(guns[0]),
      secondary: makeGun(guns[1]),
    },
    camera: makeGameCamera({
      x: 0,
      y: 0,
      viewportWidth: config.scale.width,
      viewportHeight: config.scale.height,
      bounds: {
        left: 0,
        right: 1000,
        top: 0,
        bottom: 1000,
      },
    }),
    state: GameStates.Play,
    controls: [],
    inputEvents: [],
    level: 1,
    killsCount: 0,
    items: [],
    coins: 0,
  };
} 