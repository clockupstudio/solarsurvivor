import { Character } from "../character/character";
import { GameState } from "../interfaces";
import { Vector2 } from "../character/character";
import { GameCamera } from "../camera/camera";

const calculateSpawnDistance = (camera: GameCamera): number => 
  Math.sqrt(Math.pow(camera.viewportWidth/2, 2) + Math.pow(camera.viewportHeight/2, 2)) + 100;

const generateRandomAngle = (): number => 
  Math.random() * Math.PI * 2;

const calculatePosition = (player: Character, distance: number, angle: number): Vector2 => ({
  x: player.x + Math.cos(angle) * distance,
  y: player.y + Math.sin(angle) * distance
});

const clampToBounds = (position: Vector2, bounds: GameCamera['bounds']): Vector2 => ({
  x: Math.max(bounds.left, Math.min(position.x, bounds.right)),
  y: Math.max(bounds.top, Math.min(position.y, bounds.bottom))
});

export function spawnEnemy(gameState: GameState, enemyData: Character): Character {
  const spawnDistance = calculateSpawnDistance(gameState.camera);
  const angle = generateRandomAngle();
  const position = calculatePosition(gameState.player, spawnDistance, angle);
  const clampedPosition = clampToBounds(position, gameState.camera.bounds);

  return {
    ...enemyData,
    ...clampedPosition
  };
}