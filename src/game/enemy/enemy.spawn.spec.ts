import { GameCamera } from "../camera/camera";
import { Character, Vector2 } from "../character/character";
import { GameState } from "../interfaces";
import { makeGameState } from "../states/gamestate";
import { spawnEnemy } from "./enemy.spawn";

describe('spawnEnemy', () => {
    const EPSILON = 0.000001;
    const WORLD_CENTER_X = 800;
    const WORLD_CENTER_Y = 600;
    const CAMERA_WIDTH = 160;
    const CAMERA_HEIGHT = 144;
    
    const makeTestState = (overrides?: Partial<GameState>): GameState => ({
        ...makeGameState({
            player: {
                ...makeGameState().player,
                x: WORLD_CENTER_X,
                y: WORLD_CENTER_Y
            },
            camera: {
                ...makeGameState().camera,
                viewportWidth: CAMERA_WIDTH,
                viewportHeight: CAMERA_HEIGHT,
                bounds: {
                    left: 0,
                    right: WORLD_CENTER_X * 2,
                    top: 0,
                    bottom: WORLD_CENTER_Y * 2
                }
            }
        }),
        ...overrides
    });

    const calculateDistance = (point1: Vector2, point2: Vector2): number =>
        Math.sqrt(
            Math.pow(point1.x - point2.x, 2) + 
            Math.pow(point1.y - point2.y, 2)
        );

    const calculateMinSpawnDistance = (camera: GameCamera): number =>
        Math.sqrt(
            Math.pow(camera.viewportWidth/2, 2) + 
            Math.pow(camera.viewportHeight/2, 2)
        ) + 100;

    const makeTestEnemy = (overrides?: Partial<Character>): Character => ({
        ...makeGameState().player,  // Use player as base for enemy
        ...overrides
    });

    it('should spawn enemy outside camera view', () => {
        const gameState = makeTestState();
        const enemy = makeTestEnemy();
        const result = spawnEnemy(gameState, enemy);
        
        const distance = calculateDistance(result, gameState.player);
        const minDistance = calculateMinSpawnDistance(gameState.camera);

        expect(distance).toBeGreaterThanOrEqual(minDistance - EPSILON);
    });

    it('should spawn enemy within world bounds', () => {
        const gameState = makeTestState();
        const enemy = makeTestEnemy();
        
        const result = spawnEnemy(gameState, enemy);
        
        expect(result.x).toBeGreaterThanOrEqual(gameState.camera.bounds.left);
        expect(result.x).toBeLessThanOrEqual(gameState.camera.bounds.right);
        expect(result.y).toBeGreaterThanOrEqual(gameState.camera.bounds.top);
        expect(result.y).toBeLessThanOrEqual(gameState.camera.bounds.bottom);
    });

    it('should preserve enemy properties except position', () => {
        const gameState = makeTestState();
        const enemy = makeTestEnemy({
            id: "test-id",
            life: 5,
            speed: 2,
            active: true
        });
        
        const result = spawnEnemy(gameState, enemy);
        
        expect(result).toEqual(expect.objectContaining({
            id: "test-id",
            life: 5,
            speed: 2,
            active: true
        }));
    });
}); 