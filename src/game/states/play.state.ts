import { Just, Maybe } from 'purify-ts';
import { AnimationKeys } from '../character/animations';
import { activeBullet, moveBullet, setBulletSpeed } from '../character/bullet';
import { CharacterStates, makeCharacter } from '../character/character';
import { InGameControls } from '../controls';
import { updateEnemies } from '../character/enemy';
import { GameState } from '../interfaces';
import { readPlayerHit, readPlayerRecovered, updatePlayer } from '../character/player';
import { updateItems } from '../item/item';
import { GameStates } from './gamestate';
import { ISpawnEnemiesContext, spawnEnemies } from '../level/level';
import { LevelData } from '../data/level.data';
import { justDo } from '../../helpers';
import { GameEvent, GameEventNames, makeGameEvent, makeWeaponReadyEvent, findEventsByName, getEventData, WeaponReadyEventData } from '../events/event';
import { isInCamera } from '../camera/viewport';
import { updateGunReady } from '../weapon/gun';

export interface IPlayStateContext
  extends ISpawnEnemiesContext,
    ILevelRepository,
    IPlayerRecoverStarter {}

export interface ILevelRepository {
  getLevelDataByIndex(levelIndex: number): Maybe<LevelData>;
}

export interface IPlayerRecoverStarter {
  startRecoverTimer(event: GameEvent, duration?: number): void;
}

export const updatePlayState = (context: IPlayStateContext) => (gameState: GameState) => {
  return gameState.state !== GameStates.Play
    ? Just(gameState)
    : Just(gameState)
        .chain(readWeaponReadyEvent)
        .chain(destroyBullet)
        .chain(firePrimaryWeapon(context))
        .chain(updateBullet)
        .chain(readPlayerRecovered)
        .chain(readPlayerHit)
        .chain(updateItems(context))
        .chain(updatePlayer)
        .chain(updateEnemies(context))
        .chain(increaseLevel(context))
        .chain(justDo(startPlayerRecoverTimer(context)));
};

export const updateBullet = (gameState: GameState) =>
  Just({
    ...gameState,
    bullets: gameState.bullets.map((oldBullet) => {
      return moveBullet(oldBullet);
    }),
  });

export const firePrimaryWeapon = (emit: IPlayerRecoverStarter) => (state: GameState) => {
  if (!state.controls.includes(InGameControls.WEAPON_1) || !state.weaponSlots.primary.isReady) {
    return Just(state);
  }

  emit.startRecoverTimer(
    makeWeaponReadyEvent({ id: state.weaponSlots.primary.id }),
    state.weaponSlots.primary.fireRate
  );

  return Just({
    ...state,
    bullets: [
      ...state.bullets,
      setBulletSpeed(
        activeBullet(
          makeCharacter({ animationKey: AnimationKeys.BulletIdle }),
          state.player,
          state.controls
        ),
        state.weaponSlots.primary
      ),
    ],
    weaponSlots: {
      ...state.weaponSlots,
      primary: {
        ...state.weaponSlots.primary,
        isReady: false,
      },
    },
  });
};

export function destroyBullet(state: GameState) {
  return Just({
    ...state,
    bullets: state.bullets.filter((bullet) => {
      return isInCamera(bullet, state.camera);
    }),
  });
}

export const increaseLevel = (context: ILevelRepository) => (state: GameState) => {
  return context
    .getLevelDataByIndex(state.level)
    .map((levelData) => levelData.nextLevel)
    .chain((nextLevel) => {
      return state.killsCount >= nextLevel.condition
        ? Just({ ...state, level: nextLevel.nextLevel })
        : Just(state);
    });
};

const startPlayerRecoverTimer = (context: IPlayStateContext) => (state: GameState) => {
  if (state.player.state === CharacterStates.HIT)
    context.startRecoverTimer(
      makeGameEvent(GameEventNames.PLAYER_RECOVERED),
      state.player.recoverTime
    );
  return Just(state);
};

export function readWeaponReadyEvent(state: GameState): Maybe<GameState> {
  return Maybe.fromNullable(findEventsByName(state.inputEvents, GameEventNames.WEAPON_READY)[0])
    .chain((e) => Just(getEventData<WeaponReadyEventData>(e)))
    .map((weaponReady) =>
      Just({
        ...state,
        weaponSlots: {
          ...state.weaponSlots,
          primary: updateGunReady(state.weaponSlots.primary, weaponReady.id),
          secondary: updateGunReady(state.weaponSlots.secondary, weaponReady.id),
        },
      })
    )
    .orDefault(Just(state));
}
