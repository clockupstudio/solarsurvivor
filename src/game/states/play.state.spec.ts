import { Maybe } from "purify-ts";
import { AnimationKeys } from "../character/animations";
import { makeCharacter } from "../character/character";
import { GameEventNames, makeGameEvent } from "../events/event";
import { makeMockSpawnEnemiesContext } from "../level/level.mock";
import { GameStates, makeGameState, updateGame } from "./gamestate";
import { increaseLevel, updatePlayState } from "./play.state";
import { makeTestLevelData } from "../data/level.data.helpers";
import {countHitEnemies} from "../character/enemy";
import { makeGun } from '../character/bullet';
import { makeWeaponReadyEvent } from '../events/event';
import { readWeaponReadyEvent } from './play.state';

describe('updatePlayState', () => {
    it("should move bullets", () => {
        // Arrange
        const bullet = makeCharacter();
        let context = { 
            ...makeMockSpawnEnemiesContext([]), 
            getLevelDataByIndex: jest.fn().mockReturnValue(Maybe.of(makeTestLevelData())),
            startRecoverTimer: jest.fn(),
        };

        // Act
        let result = updatePlayState(context)(
          makeGameState({ 
            bullets: [bullet], 
            weaponSlots: {
              primary: makeGun(),
              secondary: makeGun(),
            }
          })
        ).extract();
    
        // Assert
        expect(result?.bullets[0].x).toBe(-1);
    });

    it("should process weapon ready events", () => {
        // Arrange
        const primaryGun = makeGun({ id: 'gun1', isReady: false });
        let context = { 
            ...makeMockSpawnEnemiesContext([]), 
            getLevelDataByIndex: jest.fn().mockReturnValue(Maybe.of(makeTestLevelData())),
            startRecoverTimer: jest.fn(),
        };

        // Act
        let result = updatePlayState(context)(
          makeGameState({ 
            weaponSlots: {
              primary: primaryGun,
              secondary: makeGun(),
            },
            inputEvents: [makeWeaponReadyEvent({ id: 'gun1' })]
          })
        ).extract();
    
        // Assert
        expect(result?.weaponSlots.primary.isReady).toBe(true);
    });

    it('should handle multiple events in the same update', () => {
        // Arrange
        const primaryGun = makeGun({ id: 'gun1', isReady: false });
        let context = { 
            ...makeMockSpawnEnemiesContext([1]), 
            getLevelDataByIndex: jest.fn().mockReturnValue(Maybe.of(makeTestLevelData())),
            startRecoverTimer: jest.fn(),
        };

        // Act
        let result = updatePlayState(context)(makeGameState({
            weaponSlots: {
              primary: primaryGun,
              secondary: makeGun(),
            },
            inputEvents: [
                makeGameEvent(
                    GameEventNames.ENEMY_SPAWN,
                    [makeCharacter({ animationKey: AnimationKeys.EnemyIdle, active: true })]
                ),
                makeWeaponReadyEvent({ id: 'gun1' })
            ]
        })).extract();

        // Assert - verify each behavior independently
        expect(result?.enemies.length).toBe(1); // Enemy spawned
        expect(result?.weaponSlots.primary.isReady).toBe(true); // Weapon ready processed
    });

    it('should not process events when game is paused', () => {
        // Arrange
        const primaryGun = makeGun({ id: 'gun1', isReady: false });
        let context = { 
            ...makeMockSpawnEnemiesContext([]), 
            getLevelDataByIndex: jest.fn().mockReturnValue(Maybe.of(makeTestLevelData())),
            startRecoverTimer: jest.fn(),
        };

        // Act
        let result = updatePlayState(context)(makeGameState({
            state: GameStates.Pause,
            weaponSlots: {
              primary: primaryGun,
              secondary: makeGun(),
            },
            inputEvents: [makeWeaponReadyEvent({ id: 'gun1' })]
        })).extract();

        // Assert - verify specific state remains unchanged
        expect(result?.weaponSlots.primary.isReady).toBe(false);
    });
});

describe('increaseLevel', ()=> {
    // Increate game's level when kills count reached condition
    // GameState{ killCount:0, level: 1}, NextLevelData{ win: 10, next: 2 } -> GameState{ killCount: 0 level: 1}
    // GameState{ killCount:10, level: 1}, NextLevelData{ win: 10, next: 2 } -> GameState{ killCount: 9 level: 1}
    // GameState{ killCount:11, level: 1}, NextLevelData{ win: 10, next: 2 } -> GameState{ killCount: 10 level: 2}
    [
        {
            name: 'should not increase level when killsCount lower than increase condition',
            gameState: makeGameState({ killsCount: 0, level: 1}),
            nextLevel: { condition: 10, nextLevel: 2,},
            expected: makeGameState({ killsCount: 0, level: 1}),
        },
        {
            name: 'should not increase level when killsCount lower than increase condition',
            gameState: makeGameState({ killsCount: 9, level: 1}),
            nextLevel: { condition: 10, nextLevel: 2,},
            expected: makeGameState({ killsCount: 9, level: 1}),
        },
        {
            name: 'should increase level when killsCount equals to increase condition',
            gameState: makeGameState({ killsCount: 10, level: 1}),
            nextLevel: { condition: 10, nextLevel: 2,},
            expected: makeGameState({ killsCount: 10, level: 2}),
        },
    ].forEach((testCase)=>{
        it(testCase.name, ()=>{
            const levelRepo = { 
                getLevelDataByIndex: jest.fn().mockReturnValue(Maybe.of(makeTestLevelData(testCase.nextLevel)))
            };

            const result = increaseLevel(levelRepo)(testCase.gameState);
    
            expect(result).toEqual(Maybe.of(makeGameState(testCase.expected)))
        });
    });
    
});

describe('countHitEnemies', ()=>{
    [
        {
            name: 'match 1 empty returns 1',
            inputEvents: [makeGameEvent(GameEventNames.ENEMY_HIT, { id: "1234" })],
            enemies: [makeCharacter({ id: "1234" })],
            expected: 1
        },
        {
            name: 'both empty returns 0',
            enemies: [],
            inputEvents: [],
            expected: 0
        },
        
    ].forEach((testCase) =>{
        it(testCase.name, ()=>{
            expect(countHitEnemies(testCase)).toEqual(testCase.expected);
        });
    });
    
});

describe('readWeaponReadyEvent', () => {
  it('should update primary weapon ready state when matching event exists', () => {
    // Arrange
    const primaryGun = makeGun({ id: 'gun1', isReady: false });
    const secondaryGun = makeGun({ id: 'gun2', isReady: false });
    const state = makeGameState({
      weaponSlots: {
        primary: primaryGun,
        secondary: secondaryGun
      },
      inputEvents: [makeWeaponReadyEvent({ id: 'gun1' })]
    });

    // Act
    const result = readWeaponReadyEvent(state);

    // Assert
    expect(result.extract()?.weaponSlots.primary.isReady).toBe(true);
    expect(result.extract()?.weaponSlots.secondary.isReady).toBe(false);
  });

  it('should update secondary weapon ready state when matching event exists', () => {
    // Arrange
    const primaryGun = makeGun({ id: 'gun1', isReady: false });
    const secondaryGun = makeGun({ id: 'gun2', isReady: false });
    const state = makeGameState({
      weaponSlots: {
        primary: primaryGun,
        secondary: secondaryGun
      },
      inputEvents: [makeWeaponReadyEvent({ id: 'gun2' })]
    });

    // Act
    const result = readWeaponReadyEvent(state);

    // Assert
    expect(result.extract()?.weaponSlots.primary.isReady).toBe(false);
    expect(result.extract()?.weaponSlots.secondary.isReady).toBe(true);
  });

  it('should not modify state when no weapon ready event exists', () => {
    // Arrange
    const primaryGun = makeGun({ id: 'gun1', isReady: false });
    const secondaryGun = makeGun({ id: 'gun2', isReady: false });
    const state = makeGameState({
      weaponSlots: {
        primary: primaryGun,
        secondary: secondaryGun
      },
      inputEvents: []
    });

    // Act
    const result = readWeaponReadyEvent(state);

    // Assert
    expect(result.extract()).toEqual(state);
  });

  it('should not modify state when weapon id does not match any gun', () => {
    // Arrange
    const primaryGun = makeGun({ id: 'gun1', isReady: false });
    const secondaryGun = makeGun({ id: 'gun2', isReady: false });
    const state = makeGameState({
      weaponSlots: {
        primary: primaryGun,
        secondary: secondaryGun
      },
      inputEvents: [makeWeaponReadyEvent({ id: 'non_existent_gun' })]
    });

    // Act
    const result = readWeaponReadyEvent(state);

    // Assert
    expect(result.extract()?.weaponSlots.primary.isReady).toBe(false);
    expect(result.extract()?.weaponSlots.secondary.isReady).toBe(false);
  });
});

