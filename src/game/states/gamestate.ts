import { Maybe, Just } from 'purify-ts';
import { justDo } from '../../helpers';
import { makeGun } from '../character/bullet';
import { SelectedGun, updateGunReady } from '../weapon/gun';
import { makeCharacter, CharacterStates } from '../character/character';
import { InGameControls } from '../controls';
import { findEventsByName, GameEventNames, getEventData } from '../events/event';
import { GameState, IGameScene, IGameOverSceneOpener } from '../interfaces';
import { updatePlayState } from './play.state';
import { makeGameCamera } from '../camera/camera';
import { syncCamera } from '../camera/sync';
import { findGunById } from '../weapon/gun';

export enum GameStates {
  Pause,
  Play,
  Dead,
}

export function makeGameState({
  player = makeCharacter(),
  enemies = [],
  bullets = [],
  guns = [],
  weaponSlots = {
    primary: makeGun(),
    secondary: makeGun(),
  },
  camera = makeGameCamera({ viewportWidth: 10, viewportHeight: 10 }),
  state = GameStates.Play,
  controls = [],
  inputEvents = [],
  level = 1,
  killsCount = 0,
  items = [],
  coins = 0,
}: Partial<GameState> = {}): GameState {
  return {
    player,
    enemies,
    bullets,
    weaponSlots,
    guns,
    camera,
    state,
    controls,
    inputEvents,
    level,
    killsCount,
    items,
    coins,
  };
}

export function updateGame(gameScene: IGameScene) {
  return (gameState: GameState) => {
    return Maybe.fromNullable(gameState)
      .chain(syncCamera(gameScene))
      .chain(readSelectedGunEvent)
      .chain(readControls(gameScene))
      .chain(justDo(enterGameOverScene(gameScene)))
      .chain(changeToPauseState)
      .chain(changeToDeadState)
      .chain(updatePlayState(gameScene))
      .chain((state) => {
        gameScene.render(state);
        gameScene.enterPauseScene(state);
        gameScene.clearInputEvents();
        return Just(state);
      });
  };
}

function changeToDeadState(gameState: GameState) {
  return Just({
    ...gameState,
    state: gameState.player.state === CharacterStates.DEAD ? GameStates.Dead : gameState.state,
  });
}

export function readControls(gameScene: IGameScene) {
  return (gameState: GameState) => Just({ ...gameState, controls: gameScene.readInput() });
}

export const changeToPauseState = (gameState: GameState): Maybe<GameState> =>
  Just({
    ...gameState,
    state: gameState.controls.includes(InGameControls.PAUSE) ? GameStates.Pause : GameStates.Play,
  });

export function readControlInputs(state: GameState, controls: InGameControls[]): GameState {
  return {
    ...state,
    controls: controls,
  };
}

export function readSelectedGunEvent(state: GameState): Maybe<GameState> {
  return Maybe.fromNullable(findEventsByName(state.inputEvents, GameEventNames.SELECTED_GUN)[0])
    .chain((e) => Just(getEventData<SelectedGun>(e)))
    .chain(justDo((e) => console.log(e)))
    .map((selectedGun) =>
      Just({
        ...state,
        weaponSlots: {
          primary: updateGunReady(findGunById(state.guns, selectedGun.weapon1Id), selectedGun.weapon1Id),
          secondary: updateGunReady(findGunById(state.guns, selectedGun.weapon2Id), selectedGun.weapon2Id),
        },
      })
    )
    .orDefault(Just(state));
}

function enterGameOverScene(scene: IGameOverSceneOpener) {
  return (value: GameState) => {
    let event = findEventsByName(value.inputEvents, GameEventNames.PLAYER_DEAD_ANIMATION_END);
    if (event.length === 0) return;

    scene.enterGameOverScene();
  };
}
