import { Just, Maybe } from "purify-ts";
import { Mock, mock } from "ts-jest-mocker";
import { makeGun } from "../character/bullet";
import { makeCharacter } from "../character/character";
import { InGameControls } from "../controls";
import { makeGameEvent, GameEventNames } from "../events/event";
import { IGameScene } from "../interfaces";
import { updateGame, makeGameState, GameStates, readSelectedGunEvent } from "./gamestate";
import { makeTestLevelData } from "../data/level.data.helpers";
import { makeGameCamera } from "../camera/camera";
import { makeSelectedGun, NOT_FOUND_GUN } from "../weapon/gun";

describe("updateGame", () => {
  let scene: Mock<IGameScene>;

  beforeEach(() => {
    scene = mockedGameScene();
  });

  it("Initialize state", () => {
    let nextState = updateGame(scene)(makeGameState()).extract();

    expect(nextState).toEqual(
      makeGameState({
        state: GameStates.Play,
        player: makeCharacter({ animationKey: "playerMovingLeft" }),
      })
    );
  });

  it("Should pause game when push PAUSE button", () => {

    scene.readInput.mockReturnValueOnce([InGameControls.PAUSE])

    let nextState = updateGame(scene)(
      makeGameState({
        state: GameStates.Play,
        bullets: [makeCharacter()],
        controls: [InGameControls.PAUSE],
      })
    ).extract();

    expect(nextState).toEqual(
      makeGameState({
        state: GameStates.Pause,
        bullets: [makeCharacter()],
        player: makeCharacter(),
        controls: [InGameControls.PAUSE],
      })
    );
  });

  it("When game state pause and controls contains pause, game state should be PLAY", () => {
    scene.readInput.mockReturnValueOnce([InGameControls.PAUSE])

    let nextState = updateGame(scene)
      (makeGameState({
        bullets: [makeCharacter()],
        state: GameStates.Pause,
        controls: [],
      })
      ).extract();

    expect(nextState).toEqual(makeGameState({
      bullets: [makeCharacter()],
      state: GameStates.Pause,
      controls: [InGameControls.PAUSE],
    }));
  });

  it("Should enter Game over scene when player's dead animation end", () => {
    updateGame(scene)
      (makeGameState({
        inputEvents: [
          makeGameEvent(GameEventNames.PLAYER_DEAD_ANIMATION_END)
        ]
      }));

    expect(scene.enterGameOverScene).toHaveBeenCalled();
  });

  describe("camera syncing", () => {
    it("should sync camera position with phaser camera", () => {
      // Arrange
      const camera = makeGameCamera({
        x: 0,
        y: 0,
        viewportWidth: 800,
        viewportHeight: 600,
        bounds: {
          left: 0,
          right: 2400,
          top: 0,
          bottom: 1800
        }
      });

      const initialState = makeGameState({
        state: GameStates.Play,
        camera
      });

      scene.cameras = {
        main: {
          scrollX: 100,
          scrollY: 150
        }
      };

      // Act
      const nextState = updateGame(scene)(initialState).extract();

      // Assert
      expect(nextState?.camera.x).toBe(100);
      expect(nextState?.camera.y).toBe(150);
    });

    it("should respect camera bounds when syncing", () => {
      // Arrange
      const camera = makeGameCamera({
        x: 0,
        y: 0,
        viewportWidth: 800,
        viewportHeight: 600,
        bounds: {
          left: 0,
          right: 1000,
          top: 0,
          bottom: 1000
        }
      });

      const initialState = makeGameState({
        state: GameStates.Play,
        camera
      });

      scene.cameras = {
        main: {
          scrollX: 1200, // beyond right bound
          scrollY: -100  // beyond top bound
        }
      };

      // Act
      const nextState = updateGame(scene)(initialState).extract();

      // Assert
      expect(nextState?.camera.x).toBe(200); // 1000 - 800 (right bound - viewport width)
      expect(nextState?.camera.y).toBe(0);   // clamped to top bound
    });
  });

  function mockedGameScene() {
    const scene = mock<IGameScene>();
    scene.readInput.mockReturnValue([]);
    scene.render.mockImplementation(() => { });
    scene.enterPauseScene.mockImplementation(() => { });
    scene.clearInputEvents.mockImplementation(() => { });
    scene.startRecoverTimer.mockImplementation(() => { });
    scene.enterGameOverScene.mockImplementation(() => { });
    scene.getLevelDataByIndex.mockImplementation(() => Maybe.of(makeTestLevelData()));
    scene.cameras = {
      main: {
        scrollX: 0,
        scrollY: 0
      }
    };
    return scene;
  }
});

describe("readSelectedGunEvent", () => {
  it("empty event returns the same game state", () => {
    expect(readSelectedGunEvent(makeGameState())).toEqual(
      Just(makeGameState())
    );
  });

  it("should change the gun by the given the first index", () => {
    expect(
      readSelectedGunEvent(
        makeGameState({ inputEvents: [makeGameEvent(GameEventNames.SELECTED_GUN, makeSelectedGun('gun#01', ''))], guns: [makeGun({ id: 'gun#01', name: "gun#01" })] })
      )
    ).toEqual(
      Just(
        makeGameState({
          inputEvents: [makeGameEvent(GameEventNames.SELECTED_GUN, makeSelectedGun('gun#01', ''))],
          guns: [makeGun({ id: 'gun#01', name: "gun#01" })],
          weaponSlots: {
            primary: makeGun({ id: 'gun#01', name: "gun#01" }),
            secondary: NOT_FOUND_GUN,
          },
        })
      )
    );
  });

  it("should change the gun by the given the second index", () => {
    expect(
      readSelectedGunEvent((
        makeGameState({
          inputEvents: [makeGameEvent(GameEventNames.SELECTED_GUN, makeSelectedGun('gun#02', ''))],
          guns: [makeGun({ id: 'gun#01', name: "gun#01" }), makeGun({ id: 'gun#02', name: "gun#02" })],
        })
      )
      ).extract()).toEqual(
        makeGameState({
          inputEvents: [makeGameEvent(GameEventNames.SELECTED_GUN, makeSelectedGun('gun#02', ''))],
          guns: [makeGun({ id: 'gun#01', name: "gun#01" }), makeGun({ id: 'gun#02', name: "gun#02" })],
          weaponSlots: {
            primary: makeGun({ id: 'gun#02', name: "gun#02" }),
            secondary: NOT_FOUND_GUN,
          },
        })
      );
  });
});
