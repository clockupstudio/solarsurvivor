import { palette } from "../colors";

export function getScreenCenter(camera: Phaser.Cameras.Scene2D.Camera) {
  const screenCenterX = camera.worldView.x + camera.width / 2;
  const screenCenterY = camera.worldView.y + camera.height / 2;
  return { screenCenterX, screenCenterY };
}

export const textStyles = {
  fontSize: "8px",
  fontFamily: "CustomFont",
  color: palette.brightest,
};
