import Sizer from 'phaser3-rex-plugins/templates/ui/sizer/Sizer';
import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import { PanelState } from '../../menu/panel/panel.state';
import { MenuItem } from '../../menu/menu.item';

export abstract class BasePanel extends Sizer {
  constructor(scene: Phaser.Scene, config: Sizer.IConfig) {
    super(scene, config);
  }

  public render(state: PanelState) {
    this.setVisible(state.isVisible);

    state.items.forEach((item) => {
      const indicator = this.getElement(`#${item.id}-selected`, true) as any;
      if (indicator) {
        indicator.setVisible(item.isSelected);
      }
    });
  }
}
