import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import { palette } from '../../colors';
import { makeSelectedGun, SelectedGun } from '../../game/weapon/gun';
import {
  MenuSound,
  MenuState,
  initMenuState,
  makeMenuState,
  updateMenuState,
} from '../../menu/menu.state';
import config from '../../config';
import ScrollablePanel from 'phaser3-rex-plugins/templates/ui/scrollablepanel/ScrollablePanel';
import { MenuControls } from '../../menu/controls';
import Sizer from 'phaser3-rex-plugins/templates/ui/sizer/Sizer';
import { GameState } from '../../game/interfaces';
import { SlotPanel } from './slot.panel';
import { WeaponPanel } from './weapon.panel';
import { MenuItem } from '../../menu/menu.item';
import { PanelType } from '../../menu/panel/panel.contants';
import { theme } from '../../theme';
import { BasePanel } from './base.panel';
import { GameCursorKeys } from '../game/GameCursorKeys';
export default class PauseScene extends Phaser.Scene {
  rexUI?: UIPlugin;
  menuState: MenuState = makeMenuState();
  panel?: Sizer;
  scroll?: ScrollablePanel;

  isReady = true;

  cursorKeys?: GameCursorKeys;
  selectSound?:
    | Phaser.Sound.NoAudioSound
    | Phaser.Sound.HTML5AudioSound
    | Phaser.Sound.WebAudioSound;
  exitSound?: Phaser.Sound.NoAudioSound | Phaser.Sound.HTML5AudioSound | Phaser.Sound.WebAudioSound;

  constructor() {
    super('PauseScene');
  }

  preload() {
    this.load.audio('select', ['assets/sounds/select.wav']);
    this.load.audio('exit', ['assets/sounds/pause.wav']);
    this.load.bitmapFont(
      'pixel2',
      'assets/computing60-bm/computing60-bm.png',
      'assets/computing60-bm/computing60-bm.xml'
    );
  }

  init(gameState: GameState) {
    this.menuState = initMenuState(gameState);
  }

  create() {
    this.cameras.main.setBackgroundColor(palette.brightest);
    this.scene.bringToTop(this);

    this.panel = this.rexUI?.add
      .sizer({
        orientation: 'y',
        width: config.scale.width,
        height: config.scale.height,
        space: { item: 2 },
      })
      .add(this.buildTopRow(this.rexUI), { expand: true, proportion: 0 })
      .add(this.buildChangeablePanel(this.rexUI), { expand: true, proportion: 0 })
      .add(this.rexUI.add.sizer(), { expand: true, proportion: 1 })
      .add(this.add.bitmapText(0, 0, theme.darkFont, 'Z to select\nSHIFT to exit'), {
        expand: false,
        proportion: 0,
      })
      .setOrigin(0, 0)
      .layout();

    this.render();
    this.cursorKeys = {
      ...this.input.keyboard?.createCursorKeys(),
      weapon1: this.input.keyboard?.addKey(Phaser.Input.Keyboard.KeyCodes.Z)!,
      weapon2: this.input.keyboard?.addKey(Phaser.Input.Keyboard.KeyCodes.X)!,
    } as GameCursorKeys;

    this.isReady = true;

    this.selectSound = this.sound.add('select', { loop: false });
    this.exitSound = this.sound.add('exit', { loop: false });
  }

  update(): void {
    this.menuState = updateMenuState(this)(this.menuState);
  }

  public play = (sound: MenuSound) => {
    switch (sound) {
      case MenuSound.SELECT:
        this.selectSound?.play();
        break;
      case MenuSound.EXIT:
        this.exitSound?.play();
        break;
    }
  };

  public scrollTo = (percentage: number) => {
    this.scroll?.setT(percentage, true);
  };

  public exit = (selected: SelectedGun) => {
    this.scene.stop();
    this.scene.resume('GameScene', selected);
  };

  public start() {
    this.isReady = false;
    this.time.addEvent({
      delay: 300,
      loop: false,
      callback: () => {
        this.isReady = true;
      },
    });
  }

  public readInput() {
    return [
      { controls: MenuControls.UP, cursor: this.cursorKeys?.up },
      { controls: MenuControls.DOWN, cursor: this.cursorKeys?.down },
      { controls: MenuControls.LEFT, cursor: this.cursorKeys?.left },
      { controls: MenuControls.RIGHT, cursor: this.cursorKeys?.right },
      { controls: MenuControls.SELECT, cursor: this.cursorKeys?.weapon1 },
      { controls: MenuControls.EXIT, cursor: this.cursorKeys?.shift },
    ]
      .filter((control) => control.cursor?.isDown)
      .map((control) => control.controls);
  }

  public render() {
    this.menuState.panelStates.forEach((state) => {
      const panel = this.panel?.getElement(`#${state.uiName}`, true) as BasePanel;
      if (panel) {
        panel.render(state);
      }
    });
  }

  private buildTopRow(ui: UIPlugin) {
    return ui.add
      .sizer({ orientation: 'x', space: { item: 4, right: 4, top: 4, left: 4, bottom: 4 } })
      .add(
        ui.add
          .sizer({ orientation: 'x', space: { item: 4 } })
          .add(this.add.bitmapText(0, 0, theme.darkFont, 'Life'), 0)
          .add(ui.add.sizer(), 1)
          .add(this.add.bitmapText(0, 0, theme.darkFont, this.menuState.life.toString()), 0),
        1
      )
      .add(
        ui.add
          .sizer({ orientation: 'x', space: { item: 4 } })
          .add(this.add.bitmapText(0, 0, theme.darkFont, '$'), 0)
          .add(ui.add.sizer(), 1)
          .add(this.add.bitmapText(0, 0, theme.darkFont, this.menuState.coin.toString()), 0),
        1
      );
  }

  private buildChangeablePanel(ui: UIPlugin) {
    const sizer = ui.add.overlapSizer({ name: 'changeable-panel' });

    this.menuState.panelStates.forEach((panelState) => {
      const Panel = panelState.panelType === PanelType.SLOT_PANEL ? SlotPanel : WeaponPanel;
      const panel = new Panel(this, ui, panelState);
      panel.setVisible(panelState.isVisible);
      sizer.add(panel, { expand: true });
    });

    return sizer;
  }
}
