import Sizer from 'phaser3-rex-plugins/templates/ui/sizer/Sizer';
import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import ScrollablePanel from 'phaser3-rex-plugins/templates/ui/scrollablepanel/ScrollablePanel';
import { PanelState } from '../../menu/panel/panel.state';
import { MenuItem } from '../../menu/menu.item';
import { theme } from '../../theme';
import { BasePanel } from './base.panel';

export class WeaponPanel extends BasePanel {
  private scroll?: ScrollablePanel;

  constructor(scene: Phaser.Scene, ui: UIPlugin, panelState: PanelState) {
    super(scene, {
      orientation: 'y',
      name: panelState.uiName,
    });
    this.add(scene.add.bitmapText(0, 0, theme.darkFont, 'Select weapon'), 0, 'left', {
      left: 2,
      top: 2,
    }).add(
      this.buildScroll(ui, panelState.items),
      1,
      'center',
      {
        left: 2,
        top: 2,
      },
      true
    );
  }

  private buildScroll(ui: UIPlugin, menuItems: MenuItem[]) {
    this.scroll = ui.add.scrollablePanel({
      scrollMode: 'vertical',
      panel: {
        child: this.buildGrid(ui, menuItems),
      },
    });
    return this.scroll;
  }

  private buildGrid(ui: UIPlugin, menuItems: MenuItem[]) {
    let grid = ui.add.gridSizer({
      column: 2,
      row: 1,
      columnProportions: 1,
    });
    menuItems.forEach((item) => {
      grid.add(this.buildMenuItem(ui, item));
    });
    grid.layout();
    return grid;
  }

  private buildMenuItem(ui: UIPlugin, item: MenuItem) {
    return ui.add
      .sizer({
        name: item.title,
        orientation: 'x',
      })
      .add(
        this.scene.add
          .bitmapText(0, 0, theme.darkFont, '*')
          .setName(`${item.id}-selected`)
          .setVisible(item.isSelected)
      )
      .add(this.scene.add.bitmapText(0, 0, theme.darkFont, item.title).setName(`${item.id}-title`));
  }
}
