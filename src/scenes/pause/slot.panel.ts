import UIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin';
import { palette } from '../../colors';
import Sizer from 'phaser3-rex-plugins/templates/ui/sizer/Sizer';
import { PanelState } from '../../menu/panel/panel.state';
import { theme } from '../../theme';
import { MenuItem } from '../../menu/menu.item';
import { BasePanel } from './base.panel';

export class SlotPanel extends BasePanel {
  constructor(scene: Phaser.Scene, ui: UIPlugin, menuState: PanelState) {
    super(scene, {
      name: 'slot-panel',
      orientation: 'x',
      space: { item: 4, right: 4, top: 4, left: 4, bottom: 4 },
    });

    menuState.items.forEach((item) => {
      this.add(
        ui.add
          .sizer({ name: item.id, orientation: 'y', space: { item: 4, top: 4, bottom: 4 } })
          .add(scene.add.bitmapText(0, 0, theme.darkFont, item.title), 0)
          .add(
            scene.add
              .bitmapText(0, 0, theme.darkFont, item.subTitle)
              .setName(`${item.id}-subtitle`),
            0
          )
          .addBackground(
            ui.add
              .roundRectangle(0, 0, 1, 1, 0)
              .setName(`${item.id}-selected`)
              .setStrokeStyle(1, parseInt(palette.darkest, 16))
              .setVisible(item.isSelected)
          ),
        1
      );
    });
  }

  public render(state: PanelState) {
    super.render(state);
    
    state.items.forEach((item) => {
      const subtitle = this.getElement(`#${item.id}-subtitle`, true) as Phaser.GameObjects.BitmapText;
      if (subtitle) {
        subtitle.setText(item.subTitle);
      }
    });
  }
}
