export enum SpriteNames {
  NormalBullet = "normalBullet",
  Player = "player",
  Enemy = "enemy",
  Item = "item",
} 