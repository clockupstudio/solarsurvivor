import { Character } from "../../game/character/character";

export default class PhaserSprite extends Phaser.GameObjects.Sprite {
  declare body: Phaser.Physics.Arcade.Body;

  public id = "";
  
  updateByCharacter(character: Character): void {
    this.id = character.id;
    this.visible = character.active;
    this.body.enable = character.active;
    this.moveSprite(character.x, character.y);
    this.updateAnimation(character.animationKey);
  }

  private moveSprite(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  private updateAnimation(animationKey: string) {
    if(this.anims.currentAnim?.key !== animationKey || !this.anims.isPlaying) this.play(animationKey)
  }
}
