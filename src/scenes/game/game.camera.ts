import { GameCamera } from "../../game/camera/camera";
import PhaserSprite from "./sprites";

export class GameSceneCamera {

    constructor(private scene: Phaser.Scene) {}

    setupCamera(playerSprite: PhaserSprite, cameraBounds: GameCamera['bounds']) {
        this.scene.physics.world.setBounds(cameraBounds.left, cameraBounds.top, cameraBounds.right, cameraBounds.bottom);
        this.scene.cameras.main.setBounds(cameraBounds.left, cameraBounds.top, cameraBounds.right, cameraBounds.bottom);
        this.scene.cameras.main.startFollow(playerSprite, true, 0.09, 0.09);
        this.scene.cameras.main.setDeadzone(100, 100);
    }
}