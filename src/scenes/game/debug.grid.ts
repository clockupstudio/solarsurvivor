export class DebugGrid {
    private graphics: Phaser.GameObjects.Graphics;
    private numbers: Phaser.GameObjects.Text[] = [];
    
    constructor(
        private scene: Phaser.Scene,
        private config: {
            gridSize?: number;
            color?: number;
            alpha?: number;
            showNumbers?: boolean;
        } = {}
    ) {
        this.graphics = scene.add.graphics();
    }

    draw() {
        const {
            gridSize = 32,
            color = 0x444444,
            alpha = 0.3,
            showNumbers = true
        } = this.config;

        this.graphics.clear();
        this.graphics.lineStyle(1, color, alpha);

        // Clear previous numbers
        this.numbers.forEach(text => text.destroy());
        this.numbers = [];

        // Draw grid and numbers
        for (let x = 0; x <= this.scene.physics.world.bounds.width; x += gridSize) {
            this.graphics.moveTo(x, 0);
            this.graphics.lineTo(x, this.scene.physics.world.bounds.height);
            
            if (showNumbers) {
                for (let y = 0; y <= this.scene.physics.world.bounds.height; y += gridSize) {
                    const text = this.scene.add.text(x + 2, y + 2, `${x},${y}`, {
                        fontSize: '6px',
                        color: '#666666'
                    });
                    this.numbers.push(text);
                }
            }
        }

        // Draw horizontal lines
        for (let y = 0; y <= this.scene.physics.world.bounds.height; y += gridSize) {
            this.graphics.moveTo(0, y);
            this.graphics.lineTo(this.scene.physics.world.bounds.width, y);
        }

        this.graphics.strokePath();
    }

    drawCameraBounds() {
        const bounds = this.scene.physics.world.bounds;
        this.graphics.lineStyle(2, 0xff0000, 0.5);
        this.graphics.strokeRect(
            bounds.x, 
            bounds.y, 
            bounds.width, 
            bounds.height
        );
    }

    destroy() {
        this.graphics.destroy();
        this.numbers.forEach(text => text.destroy());
    }
} 