export type GameCursorKeys = Phaser.Types.Input.Keyboard.CursorKeys & {
  weapon1: Phaser.Input.Keyboard.Key;
  weapon2: Phaser.Input.Keyboard.Key;
};
