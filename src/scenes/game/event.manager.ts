import { GameEvent, GameEventNames, pushUniqueEvent } from "../../game/events/event";

export class EventManager {
  private events: GameEvent[] = [];

  push(event: GameEvent) {
    this.events.push(event);
  }

  pushUnique(event: GameEvent) {
    pushUniqueEvent(this.events, event);
  }

  findByName(name: string) {
    return this.events.filter((event) => event.name === name);
  }

  clear() {
    this.events = [];
  }

  getAll() {
    return this.events;
  }
} 