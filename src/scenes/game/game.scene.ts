import { Just, Maybe } from "purify-ts";
import { AnimationKeys, animations } from "../../game/character/animations";
import { Character, makeCharacter } from "../../game/character/character";
import { InGameControls } from "../../game/controls";
import {
  GameEvent,
  GameEventNames,
  pushUniqueEvent,
  makeGameEvent,
  makePlayerHitEventData,
  makeItemHitEvent,
} from "../../game/events/event";
import { getScreenCenter } from "../screen";
import PhaserSprite from "./sprites";
import { palette } from "../../colors";
import { v4 as uuidv4 } from "uuid";
import { GameState } from "../../game/interfaces";
import { GameStates, updateGame } from "../../game/states/gamestate";
import _ from "lodash";
import { getLevelDataByIndex } from "../../game/data/level.data";
import { makeEnemyHitEvent } from "../../game/events/enemy_hit";
import { makeInitialGameState } from "../../game/initial.game";
import { SelectedGun } from "../../game/weapon/gun";
import { SpriteManager } from "./sprite.manager";
import { SpriteNames } from "./sprite.names";
import { GameSceneCamera } from "./game.camera";
import { DebugGrid } from './debug.grid';
import { GameUI } from "./game.ui";
import { GameCursorKeys } from "./GameCursorKeys";

export interface Sprite {
  x: number;
  y: number;
  visible: boolean;
  body: {
    enable: boolean;
  };
}

export default class GameScene extends Phaser.Scene {
  private bulletSpriteManager: SpriteManager;
  private enemySpriteManager: SpriteManager;
  private itemSpriteManager: SpriteManager;
  private camera: GameSceneCamera;
  private debugGrid?: DebugGrid;
  private ui: GameUI;

  playerSprite?: PhaserSprite;

  keyboardCursor?: GameCursorKeys;

  gameState?: GameState;

  inputEvents: Array<GameEvent> = [];


  constructor() {
    super("GameScene");
    this.bulletSpriteManager = new SpriteManager(this);
    this.enemySpriteManager = new SpriteManager(this);
    this.itemSpriteManager = new SpriteManager(this);
    this.camera = new GameSceneCamera(this);
    this.ui = new GameUI(this);
  }

  preload() {
    this.load.spritesheet(SpriteNames.Player, "assets/player.png", {
      frameWidth: 16,
      frameHeight: 16,
    });
    this.load.spritesheet(SpriteNames.NormalBullet, "assets/bullet.png", {
      frameWidth: 8,
      frameHeight: 8,
    });
    this.load.spritesheet(SpriteNames.Enemy, "assets/enemy.png", {
      frameWidth: 16,
      frameHeight: 16,
    });
    this.load.spritesheet(SpriteNames.Item, "assets/item.png", {
      frameWidth: 16,
      frameHeight: 16,
    });
    this.load.bitmapFont(
      "pixel",
      "assets/computing60-bm/computing60-bm-bright.png",
      "assets/computing60-bm/computing60-bm.xml"
    );
  }

  create() {
    this.ui.create();

    animations.forEach((entry) =>
      this.anims.create({
        key: entry.key,
        frames: this.anims.generateFrameNumbers(entry.frames.texture, {
          start: entry.frames.frames.start,
          end: entry.frames.frames.end,
        }),
        frameRate: entry.frameRate,
        repeat: entry.repeat,
      })
    );

    const { screenCenterX, screenCenterY } = getScreenCenter(this.cameras.main);

    this.playerSprite = new PhaserSprite(
      this,
      screenCenterX,
      screenCenterY,
      SpriteNames.Player
    );
    this.add.existing(this.playerSprite);
    this.physics.add.existing(this.playerSprite);
    this.playerSprite.on(
      `${Phaser.Animations.Events.ANIMATION_COMPLETE_KEY}${AnimationKeys.PlayerDead}`,
      () => {
        this.inputEvents.push(
          makeGameEvent(GameEventNames.PLAYER_DEAD_ANIMATION_END)
        );
      }
    );

    this.bulletSpriteManager.prepareSpritePool(1000, SpriteNames.NormalBullet);
    this.enemySpriteManager.prepareSpritePool(1000, SpriteNames.Enemy);
    this.itemSpriteManager.prepareSpritePool(1000, SpriteNames.Item);

    this.physics.add.collider(this.bulletSpriteManager.group!, this.enemySpriteManager.group!, (_, enemy) => {
      this.inputEvents.push(makeEnemyHitEvent({ id: enemy.id, damage: 1}));
    });

    this.physics.add.collider(
      this.playerSprite,
      this.enemySpriteManager.group!,
      (_, enemy) => {
        if (this.findEventsByName(GameEventNames.PLAYER_HIT).length > 0) return;

        this.inputEvents.push(
          makeGameEvent(GameEventNames.PLAYER_HIT, makePlayerHitEventData(1))
        );
        this.inputEvents.push(makeEnemyHitEvent({ id: enemy.id, damage: 0}));
      }
    );

    this.physics.add.collider(
      this.playerSprite,
      this.itemSpriteManager.group!,
      (_, item) => {
        this.inputEvents.push(makeItemHitEvent(item.id));
      }
    );

    this.keyboardCursor = { 
      ...this.input.keyboard?.createCursorKeys(), 
      weapon1: this.input.keyboard?.addKey(Phaser.Input.Keyboard.KeyCodes.Z), 
      weapon2: this.input.keyboard?.addKey(Phaser.Input.Keyboard.KeyCodes.X) 
    } as GameCursorKeys;

    this.gameState = makeInitialGameState({
      x: this.playerSprite!.x,
      y: this.playerSprite!.y
    });

    this.events.on("resume", (_: any, data: SelectedGun) => {
      this.cameras.main.setBackgroundColor(palette.darkest);
      this.inputEvents.push(makeGameEvent(GameEventNames.SELECTED_GUN, data));
      this.time.clearPendingEvents();
      if (this.gameState) this.gameState.state = GameStates.Play;
      
    });

    this.time.addEvent({
      delay: 1000,
      loop: true,
      callback: () => {
        pushUniqueEvent(
          this.inputEvents,
          makeGameEvent(GameEventNames.ENEMY_SPAWN)
        );
      },
    });

    this.camera.setupCamera(this.playerSprite, this.gameState?.camera.bounds);

    this.debugGrid = new DebugGrid(this, {
      gridSize: 16,
      showNumbers: false
    });
    this.debugGrid.draw();
    this.debugGrid.drawCameraBounds();
  }

  destroy() {
    this.debugGrid?.destroy();
    // ... other cleanup code ...
  }

  update(): void {
    this.gameState = Maybe.fromNullable(this.gameState)
      .chain(this.readInputEvents)
      .chain(updateGame(this))
      .extract();
  }

  public readInputEvents = (gameState: GameState) => {
    return Just({ ...gameState, inputEvents: this.inputEvents });
  };

  public readInput(): InGameControls[] {
    return [
      { controls: InGameControls.UP, cursor: this.keyboardCursor?.up },
      { controls: InGameControls.DOWN, cursor: this.keyboardCursor?.down },
      { controls: InGameControls.LEFT, cursor: this.keyboardCursor?.left },
      { controls: InGameControls.RIGHT, cursor: this.keyboardCursor?.right },
      { controls: InGameControls.PAUSE, cursor: this.keyboardCursor?.shift },
      { controls: InGameControls.WEAPON_1, cursor: this.keyboardCursor?.weapon1 },
      { controls: InGameControls.WEAPON_2, cursor: this.keyboardCursor?.weapon2 },
    ]
      .filter((control) => control.cursor?.isDown)
      .map((control) => control.controls);
  }

  public clearInputEvents() {
    this.inputEvents = [];
  }

  public findEventsByName(name: string) {
    return this.inputEvents.filter((gameEvent) => gameEvent.name === name);
  }

  public render = (gameState: GameState) => {
    this.playerSprite?.updateByCharacter(gameState.player);
    this.bulletSpriteManager.moveSprites(gameState.bullets);
    this.enemySpriteManager.moveSprites(gameState.enemies);
    this.itemSpriteManager.moveSprites(gameState.items);
    this.ui.update(gameState);
  };

  public enterPauseScene = (gameState: GameState) => {
    if (gameState.state === GameStates.Pause) {
      this.scene.pause(this);
      this.scene.launch("PauseScene", gameState);
    }
  };

  public startRecoverTimer(event: GameEvent, duration: number = 1000): void {
    this.time.addEvent({
      delay: duration,
      loop: false,
      callback: () => pushUniqueEvent(this.inputEvents, event),
    });
  }

  enterGameOverScene() {
    this.scene.start("GameOverScene");
  }

  public between = (min: number, max: number) => Phaser.Math.Between(min, max);
  public newUUId = () => uuidv4();
  public getLevelDataByIndex = (levelIndex: number) => getLevelDataByIndex(levelIndex);
}
