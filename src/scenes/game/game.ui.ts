import { GameState } from "../../game/interfaces";

export class GameUI {
    private debugText?: Phaser.GameObjects.BitmapText;
    private lifeText?: Phaser.GameObjects.BitmapText;

    constructor(private scene: Phaser.Scene) {}

    create() {
        this.debugText = this.scene.add.bitmapText(10, 10, "pixel", "")
        this.debugText.setScrollFactor(0);  
        this.lifeText = this.scene.add.bitmapText(4, 144 - 10, "pixel", "10")
        this.lifeText.setScrollFactor(0);
    }

    update(gameState: GameState) {
        this.debugText!.setText(`${gameState.weaponSlots.primary.name}\n${gameState.weaponSlots.primary.isReady}\n${gameState.inputEvents.map((i) => i.name)}`);
        this.lifeText!.setText(`HP:${gameState.player.life} $:${gameState.coins}`);
    }

}