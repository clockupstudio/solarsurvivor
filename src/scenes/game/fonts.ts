import { palette } from "../../colors";

export const NormalText = {
  fontSize: "8px",
  color: palette.darkest,
  fontFamily: "CustomFont",
};
