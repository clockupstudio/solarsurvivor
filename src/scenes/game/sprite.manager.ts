import PhaserSprite from "./sprites";
import { Character } from "../../game/character/character";
import { SpriteNames } from "./sprite.names";

export class SpriteManager {
  private scene: Phaser.Scene;
  sprites: PhaserSprite[] = [];
  group?: Phaser.Physics.Arcade.Group;

  constructor(scene: Phaser.Scene) {
    this.scene = scene;
  }

  prepareSpritePool(numbers: number, spriteName: SpriteNames): PhaserSprite[] {
    this.group = this.scene.physics.add.group();
    this.sprites = this.createSprites(numbers, spriteName);
    return this.sprites;
  }

  private createSprites(numbers: number, spriteName: SpriteNames): PhaserSprite[] {
    if (numbers === 0) return [];

    const sprite = new PhaserSprite(this.scene, -100, -100, spriteName);
    sprite.visible = false;

    this.scene.add.existing(sprite);
    this.group?.add(sprite);
    sprite.body.enable = false;

    return this.createSprites(numbers - 1, spriteName).concat([sprite]);
  }

  moveSprites(characters: Character[]): void {
    this.sprites.forEach((sprite) => {
      sprite.visible = false;
      sprite.body.enable = false;
    });
    characters.forEach((character, i) => this.sprites[i].updateByCharacter(character));
  }
} 