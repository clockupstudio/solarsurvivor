import { Just } from "purify-ts";
import { Gun } from "../../game/bullet";
import { GameState } from "../../game/interfaces";

export interface WeaponTimer {
    update(gun: Gun): void;
    isReady(): boolean;
}

export class PhaserTimer {
  private timer?: Phaser.Time.TimerEvent;

  constructor(private scene: Phaser.Scene) {}

  update(gun: Gun) {
    if (gun.isReady === false && this.timer === undefined) {
      this.timer = this.scene.time.delayedCall(gun.fireRate, () => {
        this.timer = undefined;
      });
    }
  }

  updateMaybe(gameState: GameState) {
    return Just(this.update(gameState.gun))
  }

  isReady() {
    return this.timer === undefined;
  }
}
