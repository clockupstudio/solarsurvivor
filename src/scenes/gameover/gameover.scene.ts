import { palette } from "../../colors";
import { getScreenCenter, textStyles } from "../screen";

export default class GameOverScene extends Phaser.Scene {
  keySpace?: Phaser.Input.Keyboard.Key;

  constructor() {
    super("GameOverScene");
  }

  create() {
    const { screenCenterX, screenCenterY } = getScreenCenter(this.cameras.main);

    this.add
      .text(screenCenterX, screenCenterY, "GAMEOVER", {
        fontSize: "16px",
        fontFamily: "CustomFont",
        color: palette.brightest,
      })
      .setOrigin(0.5);

    this.add
      .text(
        screenCenterX,
        screenCenterY + 40,
        "press SPACE\nto start game",
        textStyles
      )
      .setOrigin(0.5);

    this.keySpace = this.input.keyboard?.addKey(
      Phaser.Input.Keyboard.KeyCodes.SPACE
    );
  }

  update(): void {
    if (this.keySpace?.isDown) {
      this.scene.start("GameScene");
    }
  }
}
