import 'phaser';

declare module 'phaser' {
    namespace GameObjects {
        interface GameObject {
            id: string; // or number, depending on your id type
        }
    }

    namespace Tilemaps {
        interface Tile {
            id: string;
        }
    }
}