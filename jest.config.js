module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  transform: {
    "^.+\\.ts?$": "ts-jest",
  },
  transformIgnorePatterns: ["<rootDir>/node_modules/"],
  maxWorkers: '50%',
  collectCoverageFrom: ['src/**/*.{ts,tsx}'],
};
